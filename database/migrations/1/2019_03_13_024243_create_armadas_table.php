<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArmadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('armadas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->string('name');
            $table->string('plate_number');
            $table->string('merk');
            $table->string('series');
            $table->string('color')->nullable();
            $table->string('type');
            $table->integer('odometer')->nullable();
            $table->string('year');
            $table->string('stnk_number')->nullable();
            $table->text('image_armada')->nullable();
            $table->text('image_stnk')->nullable();
            $table->date('stnk_expired')->nullable();
            $table->string('kir_number')->nullable();
            $table->text('image_kir')->nullable();
            $table->date('kir_expired')->nullable();
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('volume')->nullable();
            $table->string('max_weight')->nullable();
            $table->string('carrosserie_type')->nullable();
            $table->string('carrosserie_condition')->nullable();
            $table->longText('carrosserie_location')->nullable();
            $table->string('insurance')->nullable();
            $table->string('insurance_company')->nullable();
            $table->string('polis_type')->nullable();
            $table->string('no_polis')->nullable();
            $table->date('insurance_expired')->nullable();
            $table->date('polis_period')->nullable();
            $table->string('quality_insurance')->nullable();
            $table->longText('description_insurance')->nullable();
            $table->string('chassis_number')->nullable();
            $table->string('machine_number')->nullable();
            $table->string('number_tires')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->auditable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('armadas');
    }
}
