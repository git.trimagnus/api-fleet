<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('owner_name');
            $table->string('email');
            $table->string('address')->nullable();
            $table->string('phone');
            $table->string('npwp')->nullable();
            $table->integer('number_user')->default(0)->nullable();
            $table->integer('max_user')->default(0)->nullable();
            $table->date('due_date')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->auditable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
