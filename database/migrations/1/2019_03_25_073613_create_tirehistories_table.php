<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTirehistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tirehistories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('armada_id');
            $table->foreign('armada_id')->references('id')->on('armadas')->onDelete('cascade');
            $table->unsignedBigInteger('tirecondition_id')->nullable();
            $table->foreign('tirecondition_id')->references('id')->on('tireconditions')->onDelete('cascade');
            $table->unsignedBigInteger('tire_id');
            $table->foreign('tire_id')->references('id')->on('tires')->onDelete('cascade');
            $table->string('tire_number')->nullable();
            $table->text('image_tire')->nullable();
            $table->string('merk')->nullable();
            $table->string('production_code')->nullable();
            $table->string('serial_number')->nullable();
            $table->date('date_installation')->nullable();
            $table->string('km')->nullable();
            $table->string('thick_tire')->nullable();
            $table->string('tire_pressure')->nullable();
            $table->string('stamp')->nullable();
            $table->longText('description')->nullable();
            $table->date('date_remove')->nullable();
            $table->string('tire_age')->nullable();
            $table->auditable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tirehistories');
    }
}
