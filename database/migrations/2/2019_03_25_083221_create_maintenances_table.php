<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('armada_id');
            $table->foreign('armada_id')->references('id')->on('armadas')->onDelete('cascade');
            $table->unsignedBigInteger('maintenancetype_id');
            $table->foreign('maintenancetype_id')->references('id')->on('maintenancetypes')->onDelete('cascade');
            $table->string('reporter')->nullable();
            $table->string('no_maintenance')->unique()->nullable();
            $table->string('odometer')->nullable();
            $table->date('date_maintenance')->nullable();//
            $table->date('expected_complete')->nullable();
            $table->longText('chronologic')->nullable();
            $table->longText('description_accident')->nullable();
            $table->longText('location_accident')->nullable();
            $table->date('date_accident')->nullable();
            $table->text('image_1')->nullable();
            $table->text('image_2')->nullable();
            $table->string('vendor')->nullable();
            $table->string('performance_vendor')->nullable();
            $table->double('cost', 50, 2)->default(0)->nullable();
            $table->longText('note')->nullable();
            $table->date('actual_completion_date')->nullable();
            $table->tinyInteger('status')->default(0);//0. open, 1. onprogress 2. done
            $table->auditable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenances');
    }
}
