<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->string('no_payment')->unique()->nullable();
            $table->date('date_order')->nullable();
            $table->string('via')->nullable();
            $table->integer('number_user')->default(0)->nullable();
            $table->double('unit_price', 50, 2)->default(0)->nullable();
            $table->double('total_price', 50, 2)->default(0)->nullable();
            $table->date('date_paid')->nullable();
            $table->date('date_sendinv')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('snap_token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
