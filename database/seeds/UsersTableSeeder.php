<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'company_id' => 1,
            'role_id' => 1,
            'name' => 'Demo Super Admin',
            'phone' => '02125982911',
            'email' => 'demosuperadmin@trimagnus.com',
            'password' => Hash::make('demosuperadmin'),
            'remember_token' => str_random(40),
            "email_verified_at" => now(),
            'status' => 2
        ]);

        DB::table('users')->insert([
            'company_id' => 2,
            'role_id' => 2,
            'name' => 'Demo Admin',
            'phone' => '02125982911',
            'email' => 'demoadmin@trimagnus.com',
            'password' => Hash::make('demoadmin'),
            'remember_token' => str_random(40),
            "email_verified_at" => now(),
            'status' => 2
        ]);

        DB::table('users')->insert([
            'company_id' => 3,
            'role_id' => 1,
            'name' => 'Marlina',
            'phone' => '0000000000',
            'email' => 'marlina@digkontrol.com',
            'password' => Hash::make('marlinaDigkontrol'),
            'remember_token' => str_random(40),
            "email_verified_at" => now(),
            'status' => 2
        ]);
    }
}
