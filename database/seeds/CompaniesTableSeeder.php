<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => 'PT. Trimagnus Prima Dharma',
            'owner_name' => 'Dicky Winata',
            'email' => 'info@trimagnus.com',
            'address' => 'Jl. Kh. Mas Mansyur Kav. 35 Karet Tengsin, Tanah Abang',
            'phone' => '02125982911',
            'due_date' => null,
            'status' => 1
        ]);

        DB::table('companies')->insert([
            'name' => 'PT. Percobaan Untuk Admin',
            'owner_name' => 'Sanders',
            'email' => 'info@sanders.com',
            'address' => 'Jl. Jebluk',
            'phone' => '082583927182',
            'due_date' => null,
            'status' => 1
        ]);

        DB::table('companies')->insert([
            'name' => 'PT. Fitlogindo Mega Tama',
            'owner_name' => 'Marlina',
            'email' => 'fitlogindomegatama@gmail.com',
            'address' => 'Mansion Bougenville Fontana Tower Unit BF/27/D2 Jl. Trembesi Blok D, Kemayoran, Jakarta Utara',
            'phone' => '0000000000',
            'due_date' => null,
            'status' => 1
        ]);
        
    }
}
