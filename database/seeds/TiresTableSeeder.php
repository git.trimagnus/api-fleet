<?php

use Illuminate\Database\Seeder;

class TiresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 22; $i++) {
            DB::table('tires')->insert([
                'position' => $i
            ]);
        }
        

        DB::table('tires')->insert([
            'position' => 'serep1'
        ]);

        DB::table('tires')->insert([
            'position' => 'serep2'
        ]);
    }
}
