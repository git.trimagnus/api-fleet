<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SimtypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('simtypes')->insert([
            'name' => 'A'
        ]);

        DB::table('simtypes')->insert([
            'name' => 'C'
        ]);

        DB::table('simtypes')->insert([
            'name' => 'B1'
        ]);

        DB::table('simtypes')->insert([
            'name' => 'B3'
        ]);
    }
}
