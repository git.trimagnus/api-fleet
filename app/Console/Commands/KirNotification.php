<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Armada;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NotificationKirToAdmin;

class KirNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kir-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kir Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $armada_7 = Armada::whereRaw('CURDATE() = DATE_SUB(kir_expired, INTERVAL 7 DAY)')->get();
        $armada_6 = Armada::whereRaw('CURDATE() = DATE_SUB(kir_expired, INTERVAL 6 DAY)')->get();
        $armada_5 = Armada::whereRaw('CURDATE() = DATE_SUB(kir_expired, INTERVAL 5 DAY)')->get();
        $armada_4 = Armada::whereRaw('CURDATE() = DATE_SUB(kir_expired, INTERVAL 4 DAY)')->get();
        $armada_3 = Armada::whereRaw('CURDATE() = DATE_SUB(kir_expired, INTERVAL 3 DAY)')->get();
        $armada_2 = Armada::whereRaw('CURDATE() = DATE_SUB(kir_expired, INTERVAL 2 DAY)')->get();
        $armada_1 = Armada::whereRaw('CURDATE() = DATE_SUB(kir_expired, INTERVAL 1 DAY)')->get();

        if (count($armada_7)) {
            foreach ($armada_7 as $row) {
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $kir_expired = date('d F Y', strtotime($row->kir_expired));

                $details = [
                    'name' => $row->name,
                    'plate_number' => $row->plate_number,
                    'kir_number' => $row->kir_number,
                    'kir_expired' => $kir_expired
                ];

                Notification::send($userAdmin, new NotificationKirToAdmin($details));
            
            }
        }

        if (count($armada_6)) {
            foreach ($armada_6 as $row) {
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $kir_expired = date('d F Y', strtotime($row->kir_expired));

                $details = [
                    'name' => $row->name,
                    'plate_number' => $row->plate_number,
                    'kir_number' => $row->kir_number,
                    'kir_expired' => $kir_expired
                ];

                Notification::send($userAdmin, new NotificationKirToAdmin($details));
            
            }
        }

        if (count($armada_5)) {
            foreach ($armada_5 as $row) {
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $kir_expired = date('d F Y', strtotime($row->kir_expired));

                $details = [
                    'name' => $row->name,
                    'plate_number' => $row->plate_number,
                    'kir_number' => $row->kir_number,
                    'kir_expired' => $kir_expired
                ];

                Notification::send($userAdmin, new NotificationKirToAdmin($details));
            
            }
        }

        if (count($armada_4)) {
            foreach ($armada_4 as $row) {
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $kir_expired = date('d F Y', strtotime($row->kir_expired));

                $details = [
                    'name' => $row->name,
                    'plate_number' => $row->plate_number,
                    'kir_number' => $row->kir_number,
                    'kir_expired' => $kir_expired
                ];

                Notification::send($userAdmin, new NotificationKirToAdmin($details));
            
            }
        }

        if (count($armada_3)) {
            foreach ($armada_3 as $row) {
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $kir_expired = date('d F Y', strtotime($row->kir_expired));

                $details = [
                    'name' => $row->name,
                    'plate_number' => $row->plate_number,
                    'kir_number' => $row->kir_number,
                    'kir_expired' => $kir_expired
                ];

                Notification::send($userAdmin, new NotificationKirToAdmin($details));
            
            }
        }

        if (count($armada_2)) {
            foreach ($armada_2 as $row) {
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $kir_expired = date('d F Y', strtotime($row->kir_expired));

                $details = [
                    'name' => $row->name,
                    'plate_number' => $row->plate_number,
                    'kir_number' => $row->kir_number,
                    'kir_expired' => $kir_expired
                ];

                Notification::send($userAdmin, new NotificationKirToAdmin($details));
            
            }
        }

        if (count($armada_1)) {
            foreach ($armada_1 as $row) {
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $kir_expired = date('d F Y', strtotime($row->kir_expired));

                $details = [
                    'name' => $row->name,
                    'plate_number' => $row->plate_number,
                    'kir_number' => $row->kir_number,
                    'kir_expired' => $kir_expired
                ];

                Notification::send($userAdmin, new NotificationKirToAdmin($details));
            
            }
        }
    }
}
