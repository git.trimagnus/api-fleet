<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Models\ArmadaReminder;
use App\Notifications\NotificationReminderToUser;

class ReminderNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $armadareminder = ArmadaReminder::with(['armada', 'checklistvehicle'])
                    ->where('date_reminder', Carbon::today()->toDateString())
                    ->get();

        if (count($armadareminder)) {
            foreach ($armadareminder as $row) {
                $user = User::whereCompany_id($row->company_id)
                        ->whereIn('role_id', [2, 4])
                        ->get();

                $details = [
                    'armadareminder' => $row
                ];

                Notification::send($user, new NotificationReminderToUser($details));
            
            }
        }
    }
}
