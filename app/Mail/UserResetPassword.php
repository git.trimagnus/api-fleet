<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('notification@digkontrol.com')
                    ->markdown('emails.users.reset')
                    ->with([
                        'to' => $this->details['to'],
                        'username' => $this->details['username'],
                        'email' => $this->details['email'],
                        'url_reset' => $this->details['url_reset']
                    ]);
    }
}
