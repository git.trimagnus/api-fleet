<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserEmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    protected $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('notification@digkontrol.com')
                    ->markdown('emails.users.verification')
                    ->with([
                        'to' => $this->details['to'],
                        'name' => $this->details['name'],
                        'email' => $this->details['email'],
                        'url_activation' => $this->details['url_activation']
                    ]);

    }
}
