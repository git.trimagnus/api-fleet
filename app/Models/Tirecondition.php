<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Tirecondition extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function tiredetail()
    {
        return $this->hasOne(Tiredetail::class);
    }

    public function tirehistory()
    {
        return $this->hasOne(Tirehistory::class);
    }
}
