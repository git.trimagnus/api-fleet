<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;
use App\User;

class ItemDetail extends Model
{
    use AuditableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'item_type', 'item_code', 'item_name', 'unit', 'qty',
        'weight','dimensi','type'
    ];

    public function order()
    {
      return $this->belongsTo(Order::class);
    }

}
