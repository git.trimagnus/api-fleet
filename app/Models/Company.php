<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;
use App\User;

class Company extends Model
{
    use AuditableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'owner_name', 'email', 'address', 'phone', 'npwp',
        'number_user', 'max_user', 'due_date', 'status','file_surat_jalan','file_invoice',
    ];

    public function users()
    {
      return $this->hasMany(User::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function equipment()
    {
        return $this->hasMany(Equipment::class);
    }

    public function simtypes()
    {
        return $this->hasMany(Simtype::class);
    }

    public function armadas()
    {
        return $this->hasMany(Armada::class);
    }

    public function tires()
    {
        return $this->hasMany(Tire::class);
    }

    public function tiredetails()
    {
        return $this->hasMany(Tiredetail::class);
    }

    public function tirehistories()
    {
        return $this->hasMany(Tirehistory::class);
    }

    public function maintenances()
    {
        return $this->hasMany(Maintenance::class);
    }

    public function maintenancetypes()
    {
        return $this->hasMany(Maintenancetype::class);
    }

    public function checklistvehicles()
    {
        return $this->hasMany(Checklistvehicle::class);
    }

    public function checklistdetails()
    {
        return $this->hasMany(Checklistdetail::class);
    }

    public function odometerhistories()
    {
        return $this->hasMany(Odometerhistory::class);
    }

    public function routes()
    {
        return $this->hasMany(Route::class);
    }

    public function costs()
    {
        return $this->hasMany(Cost::class);
    }

    public function budgetdetails()
    {
        return $this->hasMany(Budgetdetail::class);
    }

    public function billingdetails()
    {
        return $this->hasMany(Billingdetail::class);
    }

    public function orderdetailcosts()
    {
        return $this->hasMany(Orderdetailcost::class);
    }

    public function armadareminders()
    {
        return $this->hasMany(ArmadaReminder::class);
    }

    public function schedulearmadas()
    {
      return $this->hasMany(Schedulearmada::class);
    }

    public function scheduleusers()
    {
      return $this->hasMany(Scheduleuser::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

}
