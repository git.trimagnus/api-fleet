<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'no_payment', 'date_order', 'via', 'number_user',
        'unit_price', 'total_price', 'date_paid', 'date_sendinv', 'status', 'snap_token'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

}