<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Tire extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    // public function tirecondition()
    // {
    //     return $this->hasOneThrough(
    //         Tirecondition::class,
    //         Tiredetail::class,
    //         'tire_id',
    //         'id',
    //         'id',
    //         'tirecondition_id'
    //     );
    // }

}
