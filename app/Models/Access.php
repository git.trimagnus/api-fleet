<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Access extends Model
{
  public $timestamps=true;
  protected $softDelete = true;
  protected $user;
  protected $table = 'ms_access';
  protected $primaryKey = 'id';
  protected $guarded = ['id','CreatedAt','UpdatedAt'];
}
