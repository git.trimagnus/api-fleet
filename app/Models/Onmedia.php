<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Onmedia extends Model
{
	protected $table='onmedias';
    protected $fillable = [
        'title', 'description', 'photo', 'video_url','show' ,'is_image'
    ];
}
