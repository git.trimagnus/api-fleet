<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Armada extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'name', 'plate_number', 'merk', 'series',
        'color', 'type', 'odometer', 'year',
        'stnk_number','image_armada', 'image_stnk', 'stnk_expired', 'kir_number', 'image_kir', 'kir_expired',
        'length', 'width', 'height', 'volume', 'max_weight', 'carrosserie_type',
        'carrosserie_condition', 'carrosserie_location', 'insurance', 'insurance_company',
        'polis_type', 'no_polis', 'insurance_expired', 'polis_period', 'quality_insurance',
        'description_insurance', 'chassis_number', 'machine_number', 'number_tires', 'status',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function tiredetails()
    {
        return $this->hasMany(Tiredetail::class);
    }

    public function tirehistories()
    {
        return $this->hasMany(Tirehistory::class);
    }

    public function maintenances()
    {
        return $this->hasMany(Maintenance::class);
    }

    public function odometerhistories()
    {
        return $this->hasMany(Odometerhistory::class);
    }

    public function checklistdetails()
    {
        return $this->hasMany(Checklistdetail::class);
    }
    
    public function checklisthistories()
    {
        return $this->hasMany(Checklisthistory::class);
    }

    public function armadareminders()
    {
        return $this->hasMany(ArmadaReminder::class);
    }

    public function schedulearmadas()
    {
      return $this->hasMany(Schedulearmada::class);
    }

}
