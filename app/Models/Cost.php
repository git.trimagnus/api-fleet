<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Cost extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'name', 'status',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function budgetdetails()
    {
        return $this->hasMany(Budgetdetail::class);
    }

    public function orderdetailcosts()
    {
        return $this->hasMany(Orderdetailcost::class);
    }
}
