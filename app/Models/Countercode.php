<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countercode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'for', 'lastcounter', 'max_date', 
    ];

}
