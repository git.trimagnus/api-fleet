<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedulearmada extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'armada_id', 'date', 'number', 'type'
    ];

    public function company()
    {
      return $this->belongsTo(Company::class);
    }

    public function armada()
    {
      return $this->belongsTo(Armada::class)->withTrashed();
    }
}
