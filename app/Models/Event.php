<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $table='events';
    protected $fillable = [
        'title', 'description', 'photo', 'video_url' ,'is_image'
    ];
}
