<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;
use App\User;

class Order extends Model
{
  use AuditableTrait;
  use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'armada_id', 'user_id', 'customer_id', 'no_order',
        'order_date', 'departure_date', 'arrival_date', 'item_type', 'item_weight',
        'item_qty', 'item_detail', 'image_item', 'image_item2','image_item3','bruto', 'disc', 'disc_value',
        'ppn', 'ppn_value', 'pph', 'pph_value', 'netto', 'payment', 'outstanding',
        'due_date', 'total_budgetvalue', 'driver_departure_date', 'driver_arrival_date',
        'driver_actual_hours', 'driver_actual_minutes', 'driver_note', 'no_sj', 'sj_date', 'no_invoice',
        'invoice_date', 'disciplinerating', 'servicerating', 'safetyrating', 'commentrating',
        'status_order','file_signed','order_id_digdeplus'
    ];

    protected $appends = ['duration_hours','duration_minutes'];

    public function getDurationHoursAttribute()
    {
        $dateStart = isset($this->attributes['driver_departure_date']) ? $this->attributes['driver_departure_date'] : null;
        $dateEnd = isset($this->attributes['driver_arrival_date']) ? $this->attributes['driver_arrival_date'] : null;
        $start  = new Carbon($dateStart);
        $end    = new Carbon($dateEnd);

        $hours = $end->diffInHours($start);

        return (string)$hours;
    }

    public function getDurationMinutesAttribute()
    {
        $dateStart = isset($this->attributes['driver_departure_date']) ? $this->attributes['driver_departure_date'] : null;
        $dateEnd = isset($this->attributes['driver_arrival_date']) ? $this->attributes['driver_arrival_date'] : null;
        $start  = new Carbon($dateStart);
        $end    = new Carbon($dateEnd);

        $minutes = $end->diffInMinutes($start) % 60;

        return (string)$minutes;
    }

    public function company()
    {
      return $this->belongsTo(Company::class);
    }

    public function user()
    {
      return $this->belongsTo(User::class)->withTrashed();
    }

    public function armada()
    {
      return $this->belongsTo(Armada::class)->withTrashed();
    }

    public function customer()
    {
      return $this->belongsTo(Customer::class)->withTrashed();
    }

    public function orderdetailcosts()
    {
      return $this->hasMany(Orderdetailcost::class);
    }

    public function budgetdetails()
    {
      return $this->hasMany(Budgetdetail::class);
    }

    public function itemdetails()
    {
      return $this->hasMany(Itemdetail::class);
    }

    public function billingdetails()
    {
      return $this->hasMany(Billingdetail::class);
    }

    public function routes()
    {
      return $this->hasMany(Route::class);
    }
}
