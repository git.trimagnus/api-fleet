<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class ArmadaReminder extends Model
{
    use AuditableTrait;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'armada_reminders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'armada_id', 'checklistvehicle_id', 'km', 'date_reminder',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function armada()
    {
        return $this->belongsTo(Armada::class)->withTrashed();
    }

    public function checklistvehicle()
    {
        return $this->belongsTo(Checklistvehicle::class)->withTrashed();
    }
}
