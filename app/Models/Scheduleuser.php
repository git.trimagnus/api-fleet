<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Scheduleuser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'user_id', 'date', 'number', 'type','status_order'
    ];

    public function company()
    {
      return $this->belongsTo(Company::class);
    }

    public function user()
    {
      return $this->belongsTo(User::class)->withTrashed();
    }
}
