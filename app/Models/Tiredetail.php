<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Tiredetail extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'armada_id', 'tirecondition_id', 'tire_id', 'tire_number', 'image_tire',
        'merk', 'production_code', 'serial_number', 'date_installation', 'km', 'thick_tire',
        'tire_pressure', 'stamp', 'description',
    ];

    public function tirecondition()
    {
        return $this->belongsTo(Tirecondition::class);
    }

    public function tire()
    {
        return $this->belongsTo(Tire::class);
    }
}
