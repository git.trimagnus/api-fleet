<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Odometerhistory;
use App\Models\Armada;
use App\Models\Order;
use App\Models\Maintenance;

class ReportController extends Controller
{
    public function fuelReport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'armada_id' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        DB::statement( DB::raw('SET @urut1 = 0'));    
        DB::statement( DB::raw('SET @urut2 = 1'));    

        $result = DB::select( DB::raw("SELECT 
        t1.id, t1.company_id, t1.armada_id, t1.`name`, t1.date, t1.km, t1.liter AS liter, 
        IFNULL(ROUND((t1.km - t2.km) / t2.Liter, 2),'-') AS fuel_km
        FROM
        (SELECT @urut1:=@urut1+1 as Row1, t1.* FROM 
        (SELECT odometerhistories.id, odometerhistories.company_id, odometerhistories.armada_id,
        armadas.`name`, odometerhistories.date as date,odometerhistories.km, odometerhistories.liter 
        FROM odometerhistories RIGHT JOIN armadas ON odometerhistories.armada_id = armadas.id
        WHERE YEAR(odometerhistories.date) = :t1_year
        AND MONTH(odometerhistories.date) = :t1_month
        AND odometerhistories.company_id = :t1_company_id
        AND odometerhistories.armada_id = :t1_armada_id
        AND armadas.deleted_at IS NULL)t1)t1
        LEFT JOIN 
        (SELECT @urut2:=@urut2+1 as Row2, t2.* FROM 
        (SELECT odometerhistories.id, odometerhistories.company_id, odometerhistories.armada_id,
        armadas.`name`, odometerhistories.date as date,odometerhistories.km, odometerhistories.liter 
        FROM odometerhistories  RIGHT JOIN armadas ON odometerhistories.armada_id = armadas.id
        WHERE YEAR(odometerhistories.date) = :t2_year
        AND MONTH(odometerhistories.date) = :t2_month
        AND odometerhistories.company_id = :t2_company_id
        AND odometerhistories.armada_id = :t2_armada_id
        AND armadas.deleted_at IS NULL)t2)t2
        ON t1.row1 = t2.row2 ORDER BY t1.id ASC"), [
            't1_company_id' => $request->company_id,
            't1_armada_id' => $request->armada_id,
            't1_month' => $request->month,
            't1_year' => $request->year,
            't2_company_id' => $request->company_id,
            't2_armada_id' => $request->armada_id,
            't2_month' => $request->month,
            't2_year' => $request->year,
        ]);

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }

    public function armadaPerformance(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $result = DB::select(DB::raw("SELECT
                armadas.id,
                armadas.company_id,
                armadas.`name`,
                armadas.plate_number,
                IFNULL(t1.total_order, 0) as total_order,
                IFNULL(t1.total_hours, 0) as total_hours,
                IFNULL(t2.total_maintenance_rutin, 0) as total_maintenance_rutin,
                IFNULL(t2.total_maintenance_nonrutin, 0) as total_maintenance_nonrutin
                FROM
                armadas
                LEFT JOIN (
                SELECT
                    armadas.id,
                    armadas.company_id,
                    armadas.`name`,
                    COUNT( orders.no_order) AS total_order,
                    SUM(TIMESTAMPDIFF(HOUR,orders.driver_departure_date, orders.driver_arrival_date)) AS total_hours
                FROM
                    armadas
                    LEFT JOIN orders ON armadas.id = orders.armada_id 
                    WHERE YEAR(orders.order_date) = :t1_year AND MONTH(orders.order_date) = :t1_month
                    AND orders.company_id = :t1_company_id 
                    AND orders.deleted_at IS NULL
                    AND armadas.deleted_at IS NULL
                GROUP BY
                armadas.company_id, armadas.id 
                ) AS t1 ON armadas.id = t1.id
                LEFT JOIN (
                SELECT
                    armadas.id,
                    armadas.company_id,
                    armadas.`name`,
                    sum( CASE WHEN maintenances.maintenancetype_id = 1 THEN 1 ELSE 0 END ) AS total_maintenance_rutin,
                    sum( CASE WHEN maintenances.maintenancetype_id = 2 THEN 1 ELSE 0 END ) AS total_maintenance_nonrutin 
                FROM
                    armadas
                    LEFT JOIN maintenances ON armadas.id = maintenances.armada_id
                WHERE YEAR(maintenances.created_at) = :t2_year AND MONTH(maintenances.created_at) = :t2_month
                AND maintenances.company_id = :t2_company_id 
                AND maintenances.deleted_at IS NULL
                AND armadas.deleted_at IS NULL
                GROUP BY
                armadas.company_id, armadas.id 
                ) AS t2 ON armadas.id = t2.id WHERE armadas.company_id = :company_id
                 AND armadas.deleted_at IS NULL"), [
                't1_company_id' => $request->company_id,
                't1_month' => $request->month,
                't1_year' => $request->year,
                't2_company_id' => $request->company_id,
                't2_month' => $request->month,
                't2_year' => $request->year,
                'company_id' => $request->company_id
                ]);

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);
    }

    public function insurancePerformance(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        
        $result = Armada::select(DB::raw('company_id, insurance_company,
                    count(polis_type) AS total_polis,
                    count(id) AS total_armada,
                    sum( CASE WHEN quality_insurance = "bagus" THEN 1 ELSE 0 END ) AS total_good_rate,
                    sum( CASE WHEN quality_insurance = "cukup" THEN 1 ELSE 0 END ) AS total_fair_rate,
                    sum( CASE WHEN quality_insurance = "kurang" THEN 1 ELSE 0 END ) AS total_bad_rate'))
                    ->whereCompany_id($request->company_id)
                    ->whereNotNull('insurance_company')
                    ->whereInsurance('ada')
                    ->groupBy('insurance_company')
                    ->orderBy('insurance_company', 'ASC')
                    ->get();

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }

    public function deliveryOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'armada_id' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Progress :
        // 0. Open
        // 1. On progress
        // Pending :
        // 6. Pending
        // 3. Verified
        // 2. Delivered
        // History :
        // 4. Closed
        // 5. Cancel

        $query = Order::query();
            $query = $query->select(DB::raw('orders.company_id, orders.armada_id, armadas.name,
                count(orders.customer_id) as total_customer,
                count(orders.no_order) as total_order,
                sum( CASE WHEN orders.status_order = 0 
                OR orders.status_order = 1 
                OR orders.status_order = 2
                OR orders.status_order = 3 
                THEN 1 ELSE 0 END ) AS total_process,
                sum( CASE WHEN orders.status_order = 5 
                THEN 1 ELSE 0 END ) AS total_cancel,
                sum( CASE WHEN orders.status_order = 6 
                THEN 1 ELSE 0 END ) AS total_pending,
                sum( CASE WHEN orders.status_order = 4 
                THEN 1 ELSE 0 END ) AS total_closed,
                SUM(orders.bruto) as monthly_revenue'))
                ->rightJoin('armadas', 'orders.armada_id', '=', 'armadas.id');   

        if ($request->has('company_id')) {
            $query = $query->where('orders.company_id', $request->company_id);
        }

        if (!empty($request->armada_id)) {
            $query = $query->where('orders.armada_id', $request->armada_id);
        }

        $query = $query->whereMonth('orders.order_date', $request->month);
        $query = $query->whereYear('orders.order_date', $request->year);
        $query = $query->groupBy('orders.company_id', 'orders.armada_id');
        $result = $query->get();

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }

    public function workshopPerformance(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'updated_by' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        
        $result = Maintenance::select(DB::raw('company_id,
                    updated_by,
                    COUNT( CASE WHEN expected_complete <= actual_completion_date THEN 1 ELSE NULL END ) +
                    COUNT( CASE WHEN expected_complete > actual_completion_date THEN 1 ELSE NULL END )  AS total_maintenance,
                    COUNT( CASE WHEN expected_complete <= actual_completion_date THEN 1 ELSE NULL END ) AS total_ontime,
                    COUNT( CASE WHEN expected_complete > actual_completion_date THEN 1 ELSE NULL END ) AS total_non_ontime'))
                    ->whereCompany_id($request->company_id)
                    ->whereUpdated_by($request->updated_by)
                    ->whereMonth('created_at', $request->month)
                    ->whereYear('created_at', $request->year)
                    ->groupBy('company_id', 'updated_by')
                    ->get();

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }

    public function externalWorkshop(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        
        $result = Maintenance::select(DB::raw('company_id, vendor,
                    count(no_maintenance) AS total_repair,
                    sum( CASE WHEN performance_vendor = "bagus" THEN 1 ELSE 0 END ) AS total_good_rate,
                    sum( CASE WHEN performance_vendor = "cukup" THEN 1 ELSE 0 END ) AS total_fair_rate,
                    sum( CASE WHEN performance_vendor = "kurang" THEN 1 ELSE 0 END ) AS total_bad_rate'))
                    ->whereCompany_id($request->company_id)
                    ->whereNotNull('vendor')
                    ->whereMonth('created_at', $request->month)
                    ->whereYear('created_at', $request->year)
                    ->groupBy('company_id', 'vendor')
                    ->orderBy('vendor', 'ASC')
                    ->get();

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }

    public function ratingDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'user_id' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        
        $result = Order::select(DB::raw('orders.company_id,
                orders.user_id, users.name, COUNT( orders.no_order ) AS total_order,
                IFNULL(ROUND(sum( orders.disciplinerating ) / COUNT( orders.no_order ),2),0) AS avg_disciplinerating,
                IFNULL(ROUND(sum( orders.servicerating ) / COUNT( orders.no_order ),2),0) AS avg_servicerating,
                IFNULL(ROUND(sum( orders.safetyrating ) / COUNT( orders.no_order ),2),0) AS avg_safetyrating,
                IFNULL(ROUND(((sum( orders.disciplinerating ) / COUNT( orders.no_order )) +
                (sum( orders.servicerating ) / COUNT( orders.no_order )) + 
                (sum( orders.safetyrating ) / COUNT( orders.no_order ))) / 3,2),0) AS total_avg'))
                ->rightJoin('users', 'orders.user_id', '=', 'users.id')
                ->where('orders.company_id', $request->company_id)
                ->where('orders.user_id', $request->user_id)
                ->whereMonth('orders.order_date', $request->month)
                ->whereYear('orders.order_date', $request->year)
                ->groupBy('orders.company_id', 'orders.user_id')
                ->get();

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }

    public function outstandingDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'user_id' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        
        $query = Order::query();
        $query = $query->select(DB::raw('orders.company_id,
                orders.user_id, users.name,
                orders.no_order, orders.order_date,
                orders.total_budgetvalue,
                SUM( budgetdetails.realization ) AS total_realization,
                orders.total_budgetvalue - SUM( budgetdetails.realization ) AS total_difference'))
                ->leftJoin('budgetdetails', 'orders.id', '=', 'budgetdetails.order_id')
                ->rightJoin('users', 'orders.user_id', '=', 'users.id');
        
        if ($request->has('company_id')) {
            $query = $query->where('orders.company_id', $request->company_id);
        }

        if (!empty($request->user_id)) {
            $query = $query->where('orders.user_id', $request->user_id);
        }

        $query = $query->whereMonth('orders.order_date', $request->month);
        $query = $query->whereYear('orders.order_date', $request->year);
        $query = $query->whereNotIn('orders.status_order', [4]);
        $query = $query->groupBy('orders.company_id', 'orders.user_id', 'users.name',
                'orders.no_order', 'orders.order_date', 'orders.total_budgetvalue');
        $result = $query->get();

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }

    public function outstandingCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'customer_id' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Order::query();
        $query = $query->select(DB::raw('orders.company_id,
                orders.customer_id,
                customers.name,
                COUNT(orders.no_order) AS total_order,
                SUM(CASE WHEN orders.outstanding != 0 THEN orders.outstanding ELSE 0 END ) as totalamount_order,
                COUNT( CASE WHEN orders.no_invoice IS NOT NULL THEN 1 ELSE NULL END ) AS total_invoice,
                SUM( CASE WHEN orders.no_invoice IS NOT  NULL THEN orders.outstanding ELSE 0 END ) AS totalamount_inv'
        ))
            ->rightJoin('customers', 'orders.customer_id', '=', 'customers.id');

//        $query = $query->select(DB::raw('orders.company_id,
//                orders.customer_id,
//                customers.name,
//                COUNT( CASE WHEN CURDATE() <= orders.due_date THEN 1 ELSE NULL END ) AS total_dueinv,
//                SUM( CASE WHEN CURDATE() <= orders.due_date THEN orders.outstanding ELSE 0 END ) AS totalamount_dueinv,
//                COUNT( CASE WHEN CURDATE() > orders.due_date THEN 1 ELSE NULL END ) AS totalpast_dueinv,
//                SUM( CASE WHEN CURDATE() > orders.due_date THEN orders.outstanding ELSE 0 END ) AS totalamountpast_dueinv '))
//                ->rightJoin('customers', 'orders.customer_id', '=', 'customers.id');
        
        if ($request->has('company_id')) {
            $query = $query->where('orders.company_id', $request->company_id);
        }

        if (!empty($request->customer_id)) {
            $query = $query->where('orders.customer_id', $request->customer_id);
        }

//        $query = $query->where('orders.no_invoice','!=', null);
        $query = $query->whereMonth('orders.order_date', $request->month);
        $query = $query->whereYear('orders.order_date', $request->year);
        $query = $query->groupBy('orders.company_id', 'orders.customer_id', 'customers.name');
        $result = $query->get();

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }

    public function profitAndLoss(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'present|nullable',
            'armada_id' => 'present|nullable',
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        
        $result = Order::select(DB::raw('orders.company_id,
                    orders.armada_id,
                    armadas.`name`,
                    orders.no_order,
                    orders.order_date,
                    orders.netto,
                    orders.total_budgetvalue,
                    SUM( budgetdetails.realization ) AS total_realization,
                    orders.netto - SUM( budgetdetails.realization ) AS total_profit,
                    orders.total_budgetvalue - SUM( budgetdetails.realization ) AS total_difference'))
                    ->leftJoin('budgetdetails', 'orders.id', '=', 'budgetdetails.order_id')
                    ->rightJoin('armadas', 'orders.armada_id', '=', 'armadas.id')
                    ->where('orders.company_id', $request->company_id)
                    ->where('orders.armada_id', $request->armada_id)
                    ->whereMonth('orders.order_date', $request->month)
                    ->whereYear('orders.order_date', $request->year)
                    ->groupBy('orders.company_id', 'orders.armada_id', 'orders.no_order')
                    ->get();

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

    }
    
}
