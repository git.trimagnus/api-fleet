<?php

namespace App\Http\Controllers\V1\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Company;
use App\Models\Order;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'nullable|exists:companies,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = User::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereRole_id(3);
        $user = $query->get();
        $user->load(['role', 'equipment.simtype']);

        $response = [
            'status' => 'success',
            'data' => $user
        ];
        return response()->json($response, 200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|string',
            'phone' => 'present|nullable|string',
            'image_profile' => 'present|nullable|string',
            'status' => 'required|integer',
            'equipment.simtype_id' => 'required|exists:simtypes,id',
            'equipment.sim_expired' => 'required|date_format:Y-m-d',
            'equipment.no_sim' => 'required|string',
            'equipment.no_ktp' => 'required|string',
            'equipment.image_sim' => 'present|nullable|string',
            'equipment.image_ktp' =>  'present|nullable|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company = Company::whereId($request->company_id)->first();
        $number_user = $company->users()->whereIn('role_id', [2,3,4])->count();
        $max_user = $company->max_user;

        if ($number_user > $max_user) {
            return response()->json([
                'status' => 'error',
                'message' => 'You have exceeded the maximum number of users'
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_profile)) {
            $image_profile = substr($request->image_profile, strpos($request->image_profile, ",")+1);
            $image_profile_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_profile_path = public_path() . "/uploads/user/driver/profile/" . $image_profile_name;
            file_put_contents($image_profile_path, base64_decode($image_profile));
            $image_profile_url = config('global.url'). "/uploads/user/driver/profile/" . $image_profile_name;
        } else {
            $image_profile_url = config('global.url'). "/uploads/no_profile.jpg";
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->equipment['image_sim'])) {
            $image_sim = substr($request->equipment['image_sim'], strpos($request->equipment['image_sim'], ",")+1);
            $image_sim_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_sim_path = public_path() . "/uploads/user/driver/sim/" . $image_sim_name;
            file_put_contents($image_sim_path, base64_decode($image_sim));
            $image_sim_url = config('global.url'). "/uploads/user/driver/sim/" . $image_sim_name;
        } else {
            $image_sim_url = config('global.url'). "/uploads/no_image.jpg";
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->equipment['image_ktp'])) {
            $image_ktp = substr($request->equipment['image_ktp'], strpos($request->equipment['image_ktp'], ",")+1);
            $image_ktp_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_ktp_path = public_path() . "/uploads/user/driver/ktp/" . $image_ktp_name;
            file_put_contents($image_ktp_path, base64_decode($image_ktp));
            $image_ktp_url = config('global.url'). "/uploads/user/driver/ktp/" . $image_ktp_name;
        } else {
            $image_ktp_url = config('global.url'). "/uploads/no_image.jpg";
        }

        $user = User::create([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'role_id' => 3,
            'remember_token' => str_random(40),
            'image_profile' => $image_profile_url,
            'status' => $request->status
        ]);

        $user->equipment()->create([
            'company_id' => $request->company_id,
            'simtype_id' => $request->equipment['simtype_id'],
            'sim_expired' => $request->equipment['sim_expired'],
            'no_sim' => $request->equipment['no_sim'],
            'no_ktp' => $request->equipment['no_ktp'],
            'image_sim' => $image_sim_url,
            'image_ktp' => $image_ktp_url
        ]);

        $data = User::with(['role', 'equipment.simtype'])
                ->whereId($user->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $data[0]
        ], 200);
 
    }

    public function storeForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|string',
            'phone' => 'present|nullable|string',
            'status' => 'required|integer',
            'equipment.simtype_id' => 'required|exists:simtypes,id',
            'equipment.sim_expired' => 'required|date_format:Y-m-d',
            'equipment.no_sim' => 'required|string',
            'equipment.no_ktp' => 'required|string',
            'attachments.image_profile' => 'nullable|image|mimes:jpeg,png,jpg',
            'attachments.image_sim' => 'nullable|image|mimes:jpeg,png,jpg',
            'attachments.image_ktp' =>  'nullable|image|mimes:jpeg,png,jpg',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $attachments = [];

        if ($request->hasFile('attachments.image_profile')) {
            $image_profile_name = time().'.'.request()->attachments['image_profile']->getClientOriginalExtension();
            $image_profile_path = public_path() . "/uploads/user/driver/profile/";
            request()->attachments['image_profile']->move($image_profile_path, $image_profile_name);
            $image_profile_url = config('global.url'). "/uploads/user/driver/profile/" . $image_profile_name;
            array_push($attachments, [
                'image_profile' => $image_profile_url
            ]);
        } else {
            array_push($attachments, [
                'image_profile' => null
            ]);
        }
        
        if ($request->hasFile('attachments.image_sim')) {
            $image_sim_name = time().'.'.request()->attachments['image_sim']->getClientOriginalExtension();
            $image_sim_path = public_path() . "/uploads/user/driver/sim/";
            request()->attachments['image_sim']->move($image_sim_path, $image_sim_name);
            $image_sim_url = config('global.url'). "/uploads/user/driver/sim/" . $image_sim_name;
            array_push($attachments, [
                'image_sim' => $image_sim_url
            ]);
        } else {
            array_push($attachments, [
                'image_sim' => null
            ]);
        }

        if ($request->hasFile('attachments.image_ktp')) {
            $image_ktp_name = time().'.'.request()->attachments['image_ktp']->getClientOriginalExtension();
            $image_ktp_path = public_path() . "/uploads/user/driver/ktp/";
            request()->attachments['image_ktp']->move($image_ktp_path, $image_ktp_name);
            $image_ktp_url = config('global.url'). "/uploads/user/driver/ktp/" . $image_ktp_name;
            array_push($attachments, [
                'image_ktp' => $image_ktp_url
            ]);
        } else {
            array_push($attachments, [
                'image_ktp' => null
            ]);
        }

        $user = User::create([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'role_id' => 3,
            'remember_token' => str_random(40),
            'image_profile' => $attachments[0]['image_profile'],
            'status' => $request->status
        ]);

        $user->equipment()->create([
            'company_id' => $request->company_id,
            'simtype_id' => $request->equipment['simtype_id'],
            'sim_expired' => $request->equipment['sim_expired'],
            'no_sim' => $request->equipment['no_sim'],
            'no_ktp' => $request->equipment['no_ktp'],
            'image_sim' => $attachments[1]['image_sim'],
            'image_ktp' => $attachments[2]['image_ktp'],
        ]);

        $data = User::with(['role', 'equipment.simtype'])
                ->whereId($user->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $data[0]
        ], 200);
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with(['role', 'equipment.simtype'])
                ->whereRole_id(3)
                ->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $user
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'name' => 'required|string',
            // 'email' => 'required|email|unique:users,email',
            // 'password' => 'required|min:8|string',
            'phone' => 'present|nullable|string',
            'image_profile' => 'present|nullable|string',
            'status' => 'required|integer',
            'equipment.simtype_id' => 'required|exists:simtypes,id',
            'equipment.sim_expired' => 'required|date_format:Y-m-d',
            'equipment.no_sim' => 'required|string',
            'equipment.no_ktp' => 'required|string',
            'equipment.image_sim' => 'present|nullable|string',
            'equipment.image_ktp' =>  'present|nullable|string',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::whereRole_id(3)->findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_profile)) {
            $image_profile = substr($request->image_profile, strpos($request->image_profile, ",")+1);
            $image_profile_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_profile_path = public_path() . "/uploads/user/driver/profile/" . $image_profile_name;
            file_put_contents($image_profile_path, base64_decode($image_profile));
            $image_profile_url = config('global.url'). "/uploads/user/driver/profile/" . $image_profile_name;
            $user->update([
                'image_profile' => $image_profile_url,
            ]);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->equipment['image_sim'])) {
            $image_sim = substr($request->equipment['image_sim'], strpos($request->equipment['image_sim'], ",")+1);
            $image_sim_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_sim_path = public_path() . "/uploads/user/driver/sim/" . $image_sim_name;
            file_put_contents($image_sim_path, base64_decode($image_sim));
            $image_sim_url = config('global.url'). "/uploads/user/driver/sim/" . $image_sim_name;
            $user->equipment()->update([
                'image_sim' => $image_sim_url,
            ]);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->equipment['image_ktp'])) {
            $image_ktp = substr($request->equipment['image_ktp'], strpos($request->equipment['image_ktp'], ",")+1);
            $image_ktp_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_ktp_path = public_path() . "/uploads/user/driver/ktp/" . $image_ktp_name;
            file_put_contents($image_ktp_path, base64_decode($image_ktp));
            $image_ktp_url = config('global.url'). "/uploads/user/driver/ktp/" . $image_ktp_name;
            $user->equipment()->update([
                'image_ktp' => $image_ktp_url,
            ]);
        }

        $user->update([
            'company_id' => $request->company_id,
            'name' => $request->name,
            // 'email' => $request->email,
            // 'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'role_id' => 3,
            // 'image_profile' => $image_profile_url,
            'status' =>$request->status
        ]);

        $user->equipment()->update([
            'company_id' => $request->company_id,
            'simtype_id' => $request->equipment['simtype_id'],
            'sim_expired' => $request->equipment['sim_expired'],
            'no_sim' => $request->equipment['no_sim'],
            'no_ktp' => $request->equipment['no_ktp'],
            // 'image_sim' => $image_sim_url,
            // 'image_ktp' => $image_ktp_url,
        ]);

        $data = User::with(['role', 'equipment.simtype'])
                ->whereId($user->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Record update successfully.',
            'data' => $data[0]
        ], 200);

    }

    public function updateForm(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'name' => 'required|string',
            // 'email' => 'required|email|unique:users,email',
            // 'password' => 'required|min:8|string',
            'phone' => 'present|nullable|string',
            'status' => 'required|integer',
            'equipment.simtype_id' => 'required|exists:simtypes,id',
            'equipment.sim_expired' => 'required|date_format:Y-m-d',
            'equipment.no_sim' => 'required|string',
            'equipment.no_ktp' => 'required|string',
            'attachments.image_sim' => 'nullable|image|mimes:jpeg,png,jpg',
            'attachments.image_ktp' =>  'nullable|image|mimes:jpeg,png,jpg',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::whereRole_id(3)->findOrFail($id);

        if ($request->hasFile('attachments.image_profile')) {
            $image_profile_name = time().'.'.request()->attachments['image_profile']->getClientOriginalExtension();
            $image_profile_path = public_path() . "/uploads/user/driver/profile/";
            request()->attachments['image_profile']->move($image_profile_path, $image_profile_name);
            $image_profile_url = config('global.url'). "/uploads/user/driver/profile/" . $image_profile_name;
            $user->update([
                'image_profile' => $image_profile_url,
            ]);

        }
        
        if ($request->hasFile('attachments.image_sim')) {
            $image_sim_name = time().'.'.request()->attachments['image_sim']->getClientOriginalExtension();
            $image_sim_path = public_path() . "/uploads/user/driver/sim/";
            request()->attachments['image_sim']->move($image_sim_path, $image_sim_name);
            $image_sim_url = config('global.url'). "/uploads/user/driver/sim/" . $image_sim_name;
            $user->equipment()->update([
                'image_sim' => $image_sim_url,
            ]);
        }

        if ($request->hasFile('attachments.image_ktp')) {
            $image_ktp_name = time().'.'.request()->attachments['image_ktp']->getClientOriginalExtension();
            $image_ktp_path = public_path() . "/uploads/user/driver/ktp/";
            request()->attachments['image_ktp']->move($image_ktp_path, $image_ktp_name);
            $image_ktp_url = config('global.url'). "/uploads/user/driver/ktp/" . $image_ktp_name;
            $user->equipment()->update([
                'image_ktp' => $image_ktp_url,
            ]);
        }

        $user->update([
            'company_id' => $request->company_id,
            'name' => $request->name,
            // 'email' => $request->email,
            // 'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'role_id' => 3,
            // 'image_profile' => $image_profile_url,
            'status' =>$request->status
        ]);

        $user->equipment()->update([
            'company_id' => $request->company_id,
            'simtype_id' => $request->equipment['simtype_id'],
            'sim_expired' => $request->equipment['sim_expired'],
            'no_sim' => $request->equipment['no_sim'],
            'no_ktp' => $request->equipment['no_ktp'],
            // 'image_sim' => $image_sim_url,
            // 'image_ktp' => $image_ktp_url,
        ]);

        $data = User::with(['role', 'equipment.simtype'])
                ->whereId($user->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Record update successfully.',
            'data' => $data[0]
        ], 200);

    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::whereRole_id(3)->findOrFail($id);

        //Validation Order
        $order = Order::whereUser_id($id)->whereNotIn('status_order', [4, 5])->get();

        if ($order != '0') {
            return response()->json([
                'status' => 'error',
                'message' => 'This user cannot be deleted. already has schedule.'
            ], 400);
        } else {
            $user->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'Record deleted successfully.'
            ], 200);

        }
        
    }
}
