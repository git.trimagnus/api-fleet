<?php

namespace App\Http\Controllers\V1;

use App\Models\Order;
use App\Models\Budgetdetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\NotificationDriverOrder;
use App\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class BudgetdetailController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $realization = Budgetdetail::whereOrder_id($id)->sum('realization');


        $order = Order::with('budgetdetails.cost')
                ->whereCompany_id($request->company_id)
                ->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $order , 'totalrealization' => $realization
        ];

        return response()->json($response, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'budgetdetails' => 'required',
            'budgetdetails.*.company_id' => 'required|exists:companies,id',
            'budgetdetails.*.order_id' => 'required|exists:orders,id',
            'budgetdetails.*.cost_id' => 'required|exists:costs,id',
            'budgetdetails.*.value' => 'required|numeric',
            'budgetdetails.*.realization' => 'required|numeric',
            'budgetdetails.*.note' => 'present|nullable|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::findOrFail($id);
        Budgetdetail::whereOrder_id($id)->forceDelete();
        
        foreach ($request->budgetdetails as $budgetdetail) {
            $order->budgetdetails()->create([
                    'company_id' => $budgetdetail['company_id'],
                    'order_id' => $budgetdetail['order_id'],
                    'cost_id' => $budgetdetail['cost_id'],
                    'value' => $budgetdetail['value'],
                    'realization' => $budgetdetail['realization'],
                    'difference' => $budgetdetail['value'] - $budgetdetail['realization'],
                    'note' => $budgetdetail['note'],
                ]);
        }

        $realization = Budgetdetail::whereOrder_id($id)->sum('realization');

        // dd($realization);

        $outstanding = $order->total_budgetvalue - $realization;

        $userAdmin = User::whereCompany_id($order->company_id)
                    ->whereRole_id(2)->get();
        $userDriver = User::whereId($order->user_id)->get();
        $merged = $userAdmin->merge($userDriver);
        $user = $merged->all();

        if ($outstanding != 0) {
            $details = [
                'no_order' => $order->no_order,
                'message' => 'Order dengan No. '.$order->no_order.' terjadi selisih antara kasbon admin dan actual cost driver. Harap mengembalikan Surat Jalan kepada Admin.'
            ];
        } else {
            $details = [
                'no_order' => $order->no_order,
                'message' => 'Order dengan No. '.$order->no_order.' Harap mengembalikan Surat Jalan kepada Admin.'
            ];
        }
        
        Notification::send($user, new NotificationDriverOrder($details));

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => Order::with('budgetdetails.cost')->findOrFail($id), 'totalrealization' => $realization
        ];
        return response()->json($response, 200);
    }
    
}