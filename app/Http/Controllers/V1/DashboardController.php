<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Maintenance;
use App\Models\Armada;
use Illuminate\Support\Facades\DB;
use App\Models\Company;

class DashboardController extends Controller
{
    public function admin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        //bulan berjalan
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;
        // return $year;

        // Progress :
        // 0. Open
        // 1. On progress
        // Pending :
        // 6. Pending
        // 3. Verified
        // 2. Delivered
        // History :
        // 4. Closed
        // 5. Cancel

        $order_ongoing = Order::whereCompany_id($request->company_id)
                ->whereMonth('order_date', $month)
                ->whereYear('order_date', $year)
                ->whereIn('status_order', [1, 2, 4, 3, 5])
                ->count();
        
        $order_pending = Order::whereCompany_id($request->company_id)
                ->whereMonth('order_date', $month)
                ->whereYear('order_date', $year)
                ->whereIn('status_order', [0, 6])
                ->count();

        // Status maintenance
        // 0. Open
        // 1. On progress
        // 2. Pending
        // 3. Done

        $maintenance_open = Maintenance::whereCompany_id($request->company_id)
                ->whereMonth('created_at', $month)
                ->whereYear('created_at', $year)
                ->whereIn('status', [0])
                ->count();
        
        $maintenance_process = Maintenance::whereCompany_id($request->company_id)
                ->whereMonth('created_at', $month)
                ->whereYear('created_at', $year)
                ->whereIn('status', [1, 2])
                ->count();

        //status Armada
        // 0. active
        // 1. non active

        $armada_active = Armada::whereCompany_id($request->company_id)
                        ->whereStatus(0)->count();

        $armada_nonactive = Armada::whereCompany_id($request->company_id)
                        ->whereStatus(1)->count();

        $invoice = Order::with('customer')
                ->whereCompany_id($request->company_id)
                ->where('outstanding', '!=', 0)
                ->orderBy('due_date','ASC')
                ->take(5)->get();

        $armada_performance = Armada::select(DB::raw('armadas.id,
                armadas.company_id,
                armadas.`name`,
                COUNT( orders.no_order ) AS total_order,
                SUM(
                TIMESTAMPDIFF( HOUR, orders.driver_departure_date, orders.driver_arrival_date )) 
                AS total_hours'))
                ->leftJoin('orders', 'armadas.id', '=', 'orders.armada_id')
                ->whereMonth('orders.order_date', $month)
                ->whereYear('orders.order_date', $year)
                ->where('armadas.company_id', $request->company_id)
                ->groupBy('armadas.company_id', 'armadas.id')
                ->orderBy('total_hours', 'DESC')
                ->take(5)
                ->get();

        $driver_performance = Order::select(DB::raw('orders.company_id,
                orders.user_id,
                users.`name`,
                COUNT( orders.no_order ) AS total_order'))
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->whereMonth('orders.order_date', $month)
                ->whereYear('orders.order_date', $year)
                ->where('orders.company_id', $request->company_id)
                ->groupBy('orders.company_id', 'orders.user_id')
                ->orderBy('total_order', 'DESC')
                ->take(5)
                ->get();



        $profit_and_loss = Order::select(DB::raw('orders.company_id,
                orders.armada_id,
                armadas.`name`,
                orders.netto,
                SUM( budgetdetails.realization ) AS total_realization,
                orders.netto - SUM( budgetdetails.realization ) AS total_profit'))
                ->Join('armadas', 'orders.armada_id', '=', 'armadas.id')
                ->Join('budgetdetails', 'orders.id', '=', 'budgetdetails.order_id')
                ->where('orders.company_id', $request->company_id)
                ->whereMonth('orders.order_date', $month)
                ->whereYear('orders.order_date', $year)
                ->groupBy('orders.company_id', 'orders.armada_id')
                ->orderBy('total_profit', 'DESC')
                ->take(10)
                ->get();

        $response = [
                'status' => 'success',
                'orders' => [
                        'ongoing' => $order_ongoing,
                        'pending' => $order_pending,
                ],
                'maintenances' => [
                        'open' => $maintenance_open,
                        'process' => $maintenance_process
                ],
                'armadas' => [
                        'active' => $armada_active,
                        'nonactive' => $armada_nonactive
                ],
                'invoices' => $invoice,
                'armada_performance' => $armada_performance,
                'driver_performance' => $driver_performance,
                'profit_and_loss' => $profit_and_loss
            ];

        return response()->json($response, 200);
    }

    public function superAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'month' => 'present|nullable',
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company = Company::select(DB::raw('COUNT(*) AS total_company,
                ( SELECT COUNT(*) FROM companies WHERE `status` = 1 AND id != 90 ) AS total_active_company,
                ( SELECT COUNT(*) FROM users WHERE role_id = 2 ) AS total_user_admin,
                ( SELECT COUNT(*) FROM users WHERE role_id = 2 AND `status` = 2 ) AS total_active_user_admin,
                ( SELECT COUNT(*) FROM users WHERE role_id = 3 ) AS total_user_driver,
                ( SELECT COUNT(*) FROM users WHERE role_id = 3 AND `status` = 2 ) AS total_active_user_driver,
                ( SELECT COUNT(*) FROM users WHERE role_id = 4 ) AS total_user_workshop,
                ( SELECT COUNT(*) FROM users WHERE role_id = 4 AND `status` = 2 ) AS total_active_user_workshop,
                ( SELECT COUNT(*) FROM customers ) AS total_customer,
                ( SELECT COUNT(*) FROM customers WHERE `status` = 1 ) AS total_active_customer,
                ( SELECT COUNT(*) FROM armadas ) AS total_armada,
                ( SELECT COUNT(*) FROM armadas WHERE `status` = 0 ) AS total_active_armada'))
                ->where('id', '!=', 90)
                ->first();

        $payment = DB::select( DB::raw("SELECT
                    payments.id,
                    payments.company_id,
                    companies.`name`,
                    companies.number_user * 79000 AS total_price,
                    companies.due_date 
                    FROM
                    payments
                    JOIN companies ON payments.company_id = companies.id 
                    WHERE
                    YEAR ( companies.due_date ) = :year 
                    AND MONTH ( companies.due_date ) = :month 
                    ORDER BY
                    due_date DESC"),
                    [
                        'year' => $request->year,
                        'month' => $request->month
                    ]);

        $response = [
                'status' => 'success',
                'company' => $company,
                'payment' => ['data' => $payment] 
            ];

        return response()->json($response, 200);
    }
}
