<?php

namespace App\Http\Controllers\V1;

use App\Models\ArmadaReminder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ArmadareminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable|exists:armadas,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = ArmadaReminder::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
  
        $armadareminder = $query->get();
        $armadareminder->load(['armada', 'checklistvehicle']);

        $response = [
            'status' => 'success',
            'data' => $armadareminder
        ];
        return response()->json($response, 200);
   

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'checklistvehicle_id' => 'required|exists:checklistvehicles,id',
            'km' => 'required',
            'date_reminder' => 'required|date_format:Y-m-d',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $armadareminder = ArmadaReminder::create($request->all());
        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $armadareminder
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        
        $armadareminder = ArmadaReminder::with(['armada', 'checklistvehicle'])
                ->whereCompany_id($request->company_id)
                ->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $armadareminder
        ];
        return response()->json($response, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'checklistvehicle_id' => 'required|exists:checklistvehicles,id',
            'km' => 'required',
            'date_reminder' => 'required|date_format:Y-m-d',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $armadareminder = ArmadaReminder::whereCompany_id($request->company_id)
                        ->whereArmada_id($request->armada_id)
                        ->findOrFail($id);
        $armadareminder->update($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => ArmadaReminder::find($id)
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);
            
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $armadareminder = ArmadaReminder::whereCompany_id($request->company_id)
                        ->findOrFail($id);
        $armadareminder->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];

        return response()->json($response, 200);
    }
}
