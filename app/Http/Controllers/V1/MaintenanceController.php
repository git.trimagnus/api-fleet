<?php

namespace App\Http\Controllers\V1;

use App\Models\Armada;
use App\Models\Maintenance;
use App\Models\Countercode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Schedulearmada;
use Carbon\CarbonPeriod;
use Carbon\Carbon;

class MaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable',
            'created_by' => 'nullable',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Maintenance::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('created_by')) {
            $query = $query->whereCreated_by($request->created_by);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->orderBy('id', 'DESC');
        $maintenance = $query->get();
        $maintenance->load('maintenancetype', 'armada');

        $response = [
            'status' => 'success',
            'data' => $maintenance
        ];
        return response()->json($response, 200);

    }

    public function maintenanceStatusOpen(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable',
            'created_by' => 'nullable',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Status maintenance
        // 0. Open
        // 1. On progress
        // 2. Pending
        // 3. Done

        $query = Maintenance::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('created_by')) {
            $query = $query->whereCreated_by($request->created_by);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status',[0]);
        $query = $query->orderBy('id', 'DESC');
        $maintenance = $query->get();
        $maintenance->load('maintenancetype', 'armada');

        $response = [
            'status' => 'success',
            'data' => $maintenance
        ];
        return response()->json($response, 200);
        
    }

    public function maintenanceStatusProgress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable',
            'created_by' => 'nullable',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Status maintenance
        // 0. Open
        // 1. On progress
        // 2. Pending
        // 3. Done

        $query = Maintenance::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('created_by')) {
            $query = $query->whereCreated_by($request->created_by);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status',[1,2]);
        $query = $query->orderBy('id', 'DESC');
        $maintenance = $query->get();
        $maintenance->load('maintenancetype', 'armada');

        $response = [
            'status' => 'success',
            'data' => $maintenance
        ];
        return response()->json($response, 200);
        
    }

    public function maintenanceStatusHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable',
            'created_by' => 'nullable',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Status maintenance
        // 0. Open
        // 1. On progress
        // 2. Pending
        // 3. Done

        $query = Maintenance::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('created_by')) {
            $query = $query->whereCreated_by($request->created_by);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status',[3]);
        $query = $query->orderBy('id', 'DESC');
        $maintenance = $query->get();
        $maintenance->load('maintenancetype', 'armada');

        $response = [
            'status' => 'success',
            'data' => $maintenance
        ];
        return response()->json($response, 200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'maintenancetype_id' => 'required|exists:maintenancetypes,id',
            'reporter' => 'present|nullable|string',
            'odometer' => 'present|nullable|string',
            'expected_complete' => 'present|nullable|date_format:Y-m-d',
            'chronologic' => 'present|nullable|string',
            'description_accident' => 'present|nullable|string',
            'location_accident' => 'present|nullable|string',
            'date_accident' => 'present|nullable|date_format:Y-m-d',
            'image_1' => 'present|nullable|string',
            'image_2' => 'present|nullable|string',
            'vendor' => 'present|nullable|string',
            'performance_vendor' => 'present|nullable|string',
            'cost' => 'present|nullable|numeric',
            'note' => 'present|nullable',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $armada = Armada::find($request->armada_id);

        //update status armada Non Active
        if ($armada) {
            $armada->update(['status' => 1]);
        }

        //get last maintenance
        $maintenance = Maintenance::whereArmada_id($request->armada_id)->latest()->first();

        if ($maintenance) {
            $date_maintenance = $maintenance->created_at;
        } else {
            $date_maintenance = now();
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_1)) {
            $image_1 = substr($request->image_1, strpos($request->image_1, ",")+1);
            $image_1_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_1_path = public_path() . "/uploads/maintenance/" . $image_1_name;
            file_put_contents($image_1_path, base64_decode($image_1));
            $image_1_url = config('global.url'). "/uploads/maintenance/" . $image_1_name;
        } else {
            $image_1_url = config('global.url'). "/uploads/no_image.jpg";
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_2)) {
            $image_2 = substr($request->image_2, strpos($request->image_2, ",")+1);
            $image_2_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_2_path = public_path() . "/uploads/maintenance/" . $image_2_name;
            file_put_contents($image_2_path, base64_decode($image_2));
            $image_2_url = config('global.url'). "/uploads/maintenance/" . $image_2_name;
        } else {
            $image_2_url = config('global.url'). "/uploads/no_image.jpg";
        }

        $no_maintenance = $this->countercodeMNT($request->company_id);
   
        $maintenance = Maintenance::create([
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'maintenancetype_id' => $request->maintenancetype_id,
            'reporter' => $request->reporter,
            'no_maintenance' => $no_maintenance,
            'odometer' => $request->odometer,
            'date_maintenance' => $date_maintenance,
            'expected_complete' => $request->expected_complete,
            'chronologic' => $request->chronologic,
            'description_accident' => $request->description_accident,
            'location_accident' => $request->location_accident,
            'date_accident' => $request->date_accident,
            'image_1' => $image_1_url,
            'image_2' => $image_2_url,
            'vendor' => $request->vendor,
            'performance_vendor' => $request->performance_vendor,
            'cost' => $request->cost,
            'note' => $request->note,
            'actual_completion_date' => $request->actual_completion_date,
            'status' => $request->status,
        ]);

        // Status maintenance
        // 0. Open
        // 1. On progress
        // 2. Pending
        // 3. Done

        if ($request->status == 0 || $request->status == 1) {
            $armada = Armada::find($maintenance->armada_id);

            //update status armada Non Active
            if ($armada) {
                $armada->update(['status' => 1]);
            }
            
            //update status 
            $maintenance->update(['status' => $request->status]);

        } elseif ($request->status == 3) {
            $armada = Armada::find($maintenance->armada_id);

            //update status armada Active
            if ($armada) {
                $armada->update(['status' => 0]);
            }
            
            //update status, actual_completion_date
            $maintenance->update([
                'actual_completion_date' => Carbon::today()->toDateString(),
                'status' => $request->status,
            ]);
        } else {
            $maintenance->update(['status' => $request->status]);
        }

        //insert Schedule booking
        $this->insertScheduleArmada($maintenance);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $maintenance
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable|exists:armadas,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Maintenance::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }
  
        $maintenance = $query->findOrFail($id);
        $maintenance->load('maintenancetype', 'armada');
        
        $response = [
            'status' => 'success',
            'data' => $maintenance
        ];
        return response()->json($response, 200);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'maintenancetype_id' => 'required|exists:maintenancetypes,id',
            'reporter' => 'present|nullable|string',
            'odometer' => 'present|nullable|string',
            'expected_complete' => 'present|nullable|date_format:Y-m-d',
            'chronologic' => 'present|nullable|string',
            'description_accident' => 'present|nullable|string',
            'location_accident' => 'present|nullable|string',
            'date_accident' => 'present|nullable|date_format:Y-m-d',
            'image_1' => 'present|nullable|string',
            'image_2' => 'present|nullable|string',
            'vendor' => 'present|nullable|string',
            'performance_vendor' => 'present|nullable|string',
            'cost' => 'present|nullable|numeric',
            'note' => 'present|nullable',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $maintenance = Maintenance::whereCompany_id($request->company_id)->findOrFail($id);

        // Status maintenance
        // 0. Open
        // 1. On progress
        // 2. Pending
        // 3. Done

        if ($request->status == 0 || $request->status == 1) {
            $armada = Armada::find($maintenance->armada_id);

            //update status armada Non Active
            if ($armada) {
                $armada->update(['status' => 1]);
            }
            
            //update status 
            $maintenance->update(['status' => $request->status]);

        } elseif ($request->status == 3) {
            $armada = Armada::find($maintenance->armada_id);

            //update status armada Active
            if ($armada) {
                $armada->update(['status' => 0]);
            }
            
            //update status, actual_completion_date
            $maintenance->update([
                'actual_completion_date' => Carbon::today()->toDateString(),
                'status' => $request->status,
            ]);
        } else {
            $maintenance->update(['status' => $request->status]);
        } 

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_1)) {
            $image_1 = substr($request->image_1, strpos($request->image_1, ",")+1);
            $image_1_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_1_path = public_path() . "/uploads/maintenance/" . $image_1_name;
            file_put_contents($image_1_path, base64_decode($image_1));
            $image_1_url = config('global.url'). "/uploads/maintenance/" . $image_1_name;
            $maintenance->update([
                'image_1' => $image_1_url,
            ]);
        } 

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_2)) {
            $image_2 = substr($request->image_2, strpos($request->image_2, ",")+1);
            $image_2_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_2_path = public_path() . "/uploads/maintenance/" . $image_2_name;
            file_put_contents($image_2_path, base64_decode($image_2));
            $image_2_url = config('global.url'). "/uploads/maintenance/" . $image_2_name;
            $maintenance->update([
                'image_2' => $image_2_url,
            ]);
        } 

        $armada = Armada::find($maintenance->armada_id);

        //update status armada Active
        if ($armada) {
            $armada->update(['status' => 0]);
        }

        $maintenance->update([
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'maintenancetype_id' => $request->maintenancetype_id,
            'reporter' => $request->reporter,
            'odometer' => $request->odometer,
            'expected_complete' => $request->expected_complete,
            'chronologic' => $request->chronologic,
            'description_accident' => $request->description_accident,
            'location_accident' => $request->location_accident,
            'date_accident' => $request->date_accident,
            // 'image_1' => $image_1_url,
            // 'image_2' => $image_2_url,
            'vendor' => $request->vendor,
            'performance_vendor' => $request->performance_vendor,
            'cost' => $request->cost,
            'note' => $request->note,
            'status' => $request->status,
        ]);

        $armada = Armada::find($request->armada_id);

        //update status armada Non Active
        if ($armada) {
            $armada->update(['status' => 1]);
        }

        //Delete then insert Schedule booking
        Schedulearmada::whereNumber($maintenance->no_maintenance)->delete();
        $this->insertScheduleArmada($maintenance);

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => Maintenance::find($id)
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);
            
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $maintenance = Maintenance::whereCompany_id($request->company_id)->findOrFail($id);

        //update status armada active
        Armada::findOrFail($maintenance->armada_id)->update(['status' => 0]);

        //Delete Schedule booking
        Schedulearmada::whereNumber($maintenance->no_maintenance)->delete();

        $maintenance->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }

    public function countercodeMNT($company_id)
    {
        $company_id = str_pad($company_id,3,0,STR_PAD_LEFT);

        $countercode = Countercode::whereType('MNT')
                    ->whereFor($company_id)
                    ->whereYear('max_date','=', date('Y'))->first();

        if(!$countercode){
            $insert = Countercode::insert([
                    'type' => 'MNT',
                    'for'=>$company_id,
                    'lastcounter'=> 1,
                    'max_date' => date('Y').'-12-31',
                ]);

            $nxt = 1;
        }
        else{ 
            $nxt = $countercode->lastcounter + 1; 
            $update = Countercode::where('id',$countercode->id)->update(['lastcounter'=>$nxt]);
        }
      
        $nxt = str_pad($nxt,4,0,STR_PAD_LEFT);
        $code = 'MNT'.'/'.$company_id.'/'.date('m').date('y').'/'.$nxt;
        
        return $code;
    }

    public function updateStatus(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'status' => 'required|integer'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $maintenance = Maintenance::whereCompany_id($request->company_id)->findOrFail($id);


        // Status maintenance
        // 0. Open
        // 1. On progress
        // 2. Pending
        // 3. Done

        if ($request->status == 0 || $request->status == 1) {
            $armada = Armada::find($maintenance->armada_id);

            //update status armada Non Active
            if ($armada) {
                $armada->update(['status' => 1]);
            }
            
            //update status 
            $maintenance->update(['status' => $request->status]);

        } elseif ($request->status == 3) {
            $armada = Armada::find($maintenance->armada_id);

            //update status armada Active
            if ($armada) {
                $armada->update(['status' => 0]);
            }
            
            //update status, actual_completion_date
            $maintenance->update([
                'actual_completion_date' => Carbon::today()->toDateString(),
                'status' => $request->status,
            ]);
        } else {
            $maintenance->update(['status' => $request->status]);
        } 

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $maintenance
        ];
        return response()->json($response, 200);
    }

    public function insertScheduleArmada($maintenance)
    {
        if ($maintenance->expected_complete != null) {
            $period = CarbonPeriod::create($maintenance->created_at, $maintenance->expected_complete);

            $data = [];
            // Iterate over the period
            foreach ($period as $date) {
                array_push($data, [
                    'company_id' => $maintenance->company_id,
                    'armada_id' => $maintenance->armada_id,
                    'date' => $date->format('Y-m-d'),
                    'number' => $maintenance->no_maintenance,
                    'type' => 'MNT',
                    'status_order' => $maintenance->status
                ]);
            }
            Schedulearmada::insert($data);
        }
        
    }

}
