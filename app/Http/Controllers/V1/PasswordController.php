<?php

namespace App\Http\Controllers\V1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Support\Facades\URL;
use App\Jobs\SendResetLinkEmail;
use App\Mail\UserResetPassword;
use Illuminate\Support\Facades\Mail;

class PasswordController extends Controller
{
    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'password' => 'required|string|min:8',
            'password_confirmation' => 'required|same:password|min:8',

        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $email = base64_decode($request->token);

        $user = User::whereEmail($email)->firstOrFail();
        $user->update(['password' => Hash::make($request->password)]);

        return response()->json([
            'status' => 'success',
            'message' => 'Password reset success.'
        ], 200);
    }

    public function sendResetLinkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $email = $request->email;
        $parts = explode('@', $email);
        $username = $parts[0];
        $token = base64_encode($request->email);

        //Temporary url 
        $url_reset = Url::temporarySignedRoute('forgot-password.reset', now()->addHours(24), [
                        'token' => $token
                    ]);
        
        $details = [
            'to' => $email,
            'username' => $username,
            'email' => $email,
            'url_reset' => $url_reset
        ];

        //send email
        Mail::to($email)->send(new UserResetPassword($details));

        // send email with queue job
        // dispatch(new SendResetLinkEmail($details));

        return response()->json([
            'status' => 'success',
            'message' => 'Email sent.',
        ], 200);
    }

    public function change(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string',
            'new_password' => 'required|string|min:8',
            'password_confirmation' => 'required|same:new_password|min:8',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::findOrfail(auth()->user()->id);

        $credentials = [
            'email' => $user->email,
            'password' => $request->old_password
        ];
        
        if (!auth()->attempt($credentials)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Your current password entered is invalid.'
            ], 400);
        }

        $user->update(['password' => Hash::make($request->password_confirmation)]);

        return response()->json([
            'status' => 'success',
            'message' => 'Password changed success.',
            'data' => auth()->user()
        ], 200);

    }

    public function changePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required|string|min:8',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::findOrfail($id);
        $user->update(['password' => Hash::make($request->new_password)]);

        return response()->json([
            'status' => 'success',
            'message' => 'Password changed success.',
            'data' =>  $user
        ], 200);

    }

}
