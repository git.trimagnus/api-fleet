<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use Carbon\Carbon;
use App\Models\Maintenance;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUs;

class HomeController extends Controller
{
    public function getAllOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::whereCompany_id($request->company_id)
                ->whereDate('order_date', Carbon::today()->toDateString())
                ->get();
        
        return response()->json([
            'status' => 'success',
            'data' => $order,
        ],200);
    }

    public function getAllMaintenances(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // 0. open, 1. onprogress, 2. pending, 3, closed
        $Maintenance = Maintenance::with(['maintenancetype', 'armada'])
                ->whereCompany_id($request->company_id)
                ->where('status', '!=', 3)
                ->get();
        
        return response()->json([
            'status' => 'success',
            'data' => $Maintenance,
        ],200);
    }

    public function getAllOrderHistories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $today = Carbon::today()->toDateString();
        $substr = substr($today,0,7);
        $order = Order::whereCompany_id($request->company_id)
                ->Where('order_date', 'like', $substr.'%')
                ->get();
        
        return response()->json([
            'status' => 'success',
            'data' => $order,
        ],200);
    }

    public function getAllMaintenanceHistories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $today = Carbon::today()->toDateString();
        $substr = substr($today,0,7);
        $maintenance = Maintenance::with(['maintenancetype', 'armada'])
                ->whereCompany_id($request->company_id)
                ->Where('date_maintenance', 'like', $substr.'%')
                ->get();
        
        return response()->json([
            'status' => 'success',
            'data' => $maintenance
        ],200);
    }

    public function getAllInvoiceHistories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $today = Carbon::today()->toDateString();
        $substr = substr($today,0,7);
        $order = Order::with('customer')
                ->whereCompany_id($request->company_id)
                ->Where('order_date', 'like', $substr.'%')
                ->Where('outstanding', '!=', 0)
                ->get();
        
        return response()->json([
            'status' => 'success',
            'data' => $order,
        ],200);
    }

    public function contactUs(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'company_name' => 'required',
            'email' => 'required|email',
            'description' => 'required' 
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $details = $request->all();
        $email = 'support@digkontrol.com';

        //send email
        Mail::to($email)->send(new ContactUs($details));

        // send email with queue job
        // dispatch(new SendEmailContactUs($details));

        return response()->json([
            'status' => 'success',
            'message' => 'Email Sent.',
        ],200);
    }
}
