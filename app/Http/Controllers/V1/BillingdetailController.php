<?php

namespace App\Http\Controllers\V1;

use App\Models\Order;
use App\Models\Billingdetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Countercode;

class BillingdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'order_id' => 'required|exists:orders,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Billingdetail::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('order_id')) {
            $query = $query->whereOrder_id($request->order_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
  
        $billingdetail = $query->get();
        $billingdetail->load('order');

        $response = [
            'status' => 'success',
            'data' => $billingdetail
        ];

        return response()->json($response, 200);
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'order_id' => 'required|exists:orders,id',
            'payment_date' => 'required|date_format:Y-m-d',
            'via' => 'required|string',
            'payment_amount' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::findOrFail($request->order_id);
        $no_payment = $this->countercodePAY($request->company_id, $order->customer_id);
        
        $order->billingdetails()->create([
            'company_id' => $request->company_id,
            'order_id' => $request->order_id,
            'no_payment' => $no_payment,
            'payment_date' => $request->payment_date,
            'via' => $request->via,
            'payment_amount' => $request->payment_amount,
        ]);
        
        //calculate payment
        $total_payment = $order->billingdetails->sum('payment_amount');
        $netto = $order->netto;
        $outstanding = $netto - $total_payment;

        $order->update([
            'payment' => $total_payment,
            'outstanding' => $outstanding
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => Billingdetail::whereOrder_id($request->order_id)->get()
        ];
        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $billingdetail = Billingdetail::with('order')
                        ->whereCompany_id($request->company_id)
                        ->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $billingdetail
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'order_id' => 'required|exists:orders,id',
            'payment_date' => 'required|date_format:Y-m-d',
            'via' => 'required|string',
            'payment_amount' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::whereCompany_id($request->company_id)->findOrFail($request->order_id);
        $billingdetail = $order->billingdetails()->findOrFail($id);
        $billingdetail->update($request->all());

        //recalculate payment
        $total_payment = $order->billingdetails->sum('payment_amount');
        $netto = $order->netto;
        $outstanding = $netto - $total_payment;
        $order->update([
            'payment' => $total_payment,
            'outstanding' => $outstanding
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record update successfully.',
            'data' => $billingdetail
        ];
        
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'order_id' => 'required|exists:orders,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::whereCompany_id($request->company_id)->findOrFail($request->order_id);
        $billingdetail = $order->billingdetails()->findOrFail($id);
        $billingdetail->delete();

        //recalculate payment
        $total_payment = $order->billingdetails->sum('payment_amount');
        $netto = $order->netto;
        $outstanding = $netto - $total_payment;
        $order->update([
            'payment' => $total_payment,
            'outstanding' => $outstanding
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record delete successfully.'
        ];
        
        return response()->json($response, 200);
    }

    public function countercodePAY($company_id, $customer_id)
    {
        $company_id = str_pad($company_id,3,0,STR_PAD_LEFT);
        $customer_id = str_pad($customer_id,3,0,STR_PAD_LEFT);
        $countercode = Countercode::whereType('PAY')
                    ->whereFor($company_id)
                    ->whereYear('max_date','=', date('Y'))->first();

        if(!$countercode){
            $insert = Countercode::insert([
                    'type' => 'PAY',
                    'for'=>$company_id,
                    'lastcounter'=> 1,
                    'max_date' => date('Y').'-12-31',
                ]);

            $nxt = 1;
        }
        else{ 
            $nxt = $countercode->lastcounter + 1; 
            $update = Countercode::where('id',$countercode->id)->update(['lastcounter'=>$nxt]);
        }

        $nxt = str_pad($nxt,4,0,STR_PAD_LEFT);
        $code = 'PAY'.'/'.$company_id.'-'.$customer_id.'/'.date('m').date('y').'/'.$nxt;
        
        return $code;
    }
}
