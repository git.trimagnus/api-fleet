<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Carbon;
use App\Models\Company;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Jobs\SendEmailVerification;
use \Illuminate\Support\Facades\URL;
use App\Mail\UserEmailVerification;
use Illuminate\Support\Facades\Mail;
use App\Models\Countercode;
use App\Models\Payment;
use App\Mail\NotificationPayment;
use App\Models\Subscription;
use App\Mail\NotificationJoin;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

//        $user = User::where('email',$request->email)->first();
//        if(Hash::check($request->password,$user->password)){
//
//        }
//        else{
//            return response()->json([
//                'status' => 'error',
//                'message' => 'These credentials do not match our records.'
//            ], 400);
//        }


        if (!$token = auth()->setTTL(20160)->attempt($credentials)) {
            return response()->json([
                'status' => 'error',
                'message' => 'These credentials do not match our records.'
            ], 400);
        }

        $user_id = auth()->user()->id;
        $role_id = auth()->user()->role_id;
        $status_company = auth()->user()->company->status;
        $status_user = auth()->user()->status;

        //check if company non active
        if ($status_company != 1) {
            return response()->json([
                'status' => 'error',
                'message' => 'Your company account has been disabled, please complete the payment.'
            ], 403);
        }

        //check if user non active
        if ($status_user != 2) {
            return response()->json([
                'status' => 'error',
                'message' => 'Your account has been disabled.'
            ], 403);
        }

        if ($request->headers->has('device')) {
            $user = User::findOrFail($user_id);
            if ($user->device != $request->header('device')) {
                // Update device on user
                $user->update([
                    'device' => $request->header('device')
                ]);
            }
        }

//        else {
//            return response()->json([
//                'status' => 'error',
//                'message' => 'Please fill your device.'
//            ], 401);
//        }

        $query = User::query();
        $query = $query->whereId($user_id);
        $user = $query->first();

        //Login as Driver
        if ($role_id == '3') {
            $user->load('role', 'company', 'equipment.simtype');
        } else {
            $user->load('role', 'company');
        }

        return response()->json([
            'status' => 'success',
            'token' => $token,
            'authorization' => 'Bearer token',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $user,
        ], 200);

    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'companies.name' => 'required',
            'companies.owner_name' => 'required',
            'companies.email' => 'required|email',
            'companies.address' => 'required',
            'companies.phone' => 'required',
            'companies.number_user' => 'required|numeric',
            'users.name' => 'required',
            'users.phone' => 'required',
            'users.email' => 'required|email|unique:users,email',
            'users.password' => 'required|string|min:8',
            'users.confirm_password' => 'required|same:users.password|min:8',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company = Company::create([
            'name' => $request->companies['name'],
            'owner_name' => $request->companies['owner_name'],
            'email' => $request->companies['email'],
            'address' => $request->companies['address'],
            'phone' => $request->companies['phone'],
            'number_user' => $request->companies['number_user'],
            'max_user' => $request->companies['number_user'] + 4,
            'npwp' => null,
            'due_date' => null,
            'status' => 0
        ]);
        $company->users()->create([
            'name' => $request->users['name'],
            'phone' => $request->users['phone'],
            'email' => $request->users['email'],
            'password' => Hash::make($request->users['password']),
            'role_id' => 2,
            'remember_token' => str_random(40),
            'status' => 0
        ]);

        $subscription = Subscription::whereRaw('? between start_date and end_date', date('Y-m-d'))->first();
        //get price from subscription
        $unit_price = $subscription->price;

        //calculate total price
        $total_price = $request->companies['number_user'] * $unit_price;

        //generate no payment
        $no_payment = $this->countercodeDIG($company->id);

        $payment = Payment::create([
            'company_id' => $company->id,
            'no_payment' => $no_payment,
            'date_order' => date("Y-m-d"),
            'via' => null,
            'number_user' => $request->companies['number_user'],
            'unit_price' => $unit_price,
            'total_price' => $total_price,
            'date_paid' => null,
            'status' => 0
        ]);

        if ($unit_price == 0) {
            $storeOrderPayment = $this->storeOrderPayment($payment);
        } else {

            $url_order_payment = Url::signedRoute('order.payment', ['no_payment' => $no_payment]);

            $details = [
                'to' => $company->email,
                'owner_name' => $company->owner_name,
                'address' => $company->address,
                'message' => 'Berikut adalah link untuk pembayaran penggunaan Digkontrol 3 bulan berikutnya. ',
                'phone' => $company->phone,
                'npwp' => $company->npwp,
                'no_payment' => $no_payment,
                'url_order_payment' => $url_order_payment
            ];

            //send email order payment
            Mail::to($company->email)->send(new NotificationPayment($details));
        }

        return response()->json([
            'status' => 'success',
            'message' => 'You have registered successfully.',
        ], 200);

    }

    public function verification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::with('company')
            ->whereEmail($request->email)
            ->first();

        $user->company()->update([
            'due_date' => Carbon::now()->addMonths(1),
            'status' => 1
        ]);

        $user->update([
            'status' => 2,
            'email_verified_at' => now()
        ]);

        return view('verified');
    }

    public function refresh()
    {
        $newToken = auth()->refresh(true, true);

        return response()->json([
            'status' => 'success',
            'message' => 'Token has been refreshed.',
            'token' => $newToken,
            'authorization' => 'Bearer token',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ], 200);
    }

    public function me()
    {
        $user = auth()->user();

        return response()->json([
            'status' => 'success',
            'user' => $user
        ], 200);
    }

    public function storeOrderPayment($data)
    {
        $payment = Payment::whereNo_payment($data->no_payment)->firstOrFail();

        // for promo
        if ($payment->total_price == 0) {
            // 1 month
            $date = Carbon::now()->addMonth()->toDateString();
            $date_sendinv = date('Y-m-d', strtotime('-7 days', strtotime($date)));

            //due date for non active company
            $due_date = date('Y-m-d', strtotime('+7 days', strtotime($date)));

            // for non promo
        } else {
            // 3 months
            $date = Carbon::now()->addMonths(3)->toDateString();
            $date_sendinv = date('Y-m-d', strtotime('-7 days', strtotime($date)));

            //due date for non active company
            $due_date = date('Y-m-d', strtotime('+7 days', strtotime($date)));
        }

        $payment->update([
            'via' => $data->via,
            'date_paid' => Carbon::today(),
            'date_sendinv' => $date_sendinv,
            'status' => 1,
        ]);

        //update status company to active
        $company = Company::findOrFail($payment->company_id);
        $company->update([
            'due_date' => $due_date,
            'status' => 1
        ]);

        //update status user
        $user = $company->users()->latest()->first();
        $user->update([
            'status' => 1
        ]);

        //signed url 
        $url_activation = URL::signedRoute('user.activation', ['email' => $user->email]);

        $details = [
            'to' => $user->email,
            'name' => $user->name,
            'email' => $user->email,
            'url_activation' => $url_activation
        ];

        //send email to Admin
        Mail::to($user->email)->send(new UserEmailVerification($details));

        $details = [
            'to' => $company->email,
            'name' => $company->name,
            'email' => $user->email,
        ];

        //send email to Company
        Mail::to($company->email)->send(new NotificationJoin($details));

    }

    public function countercodeDIG($company_id)
    {
        $company_id = str_pad($company_id, 3, 0, STR_PAD_LEFT);
        $countercode = Countercode::whereType('DIG')
            ->whereFor($company_id)
            ->whereYear('max_date', '=', date('Y'))->first();

        if (!$countercode) {
            $insert = Countercode::insert([
                'type' => 'DIG',
                'for' => $company_id,
                'lastcounter' => 1,
                'max_date' => date('Y') . '-12-31',
            ]);

            $nxt = 1;
        } else {
            $nxt = $countercode->lastcounter + 1;
            $update = Countercode::where('id', $countercode->id)->update(['lastcounter' => $nxt]);
        }

        $nxt = str_pad($nxt, 4, 0, STR_PAD_LEFT);
        $code = 'DIG' . '/' . $company_id . '/' . date('m') . date('y') . '/' . $nxt;

        return $code;
    }

    public function checkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'You can use this email.',
            ], 200);
        }
    }

}
