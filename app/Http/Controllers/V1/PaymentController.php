<?php

namespace App\Http\Controllers\V1;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Company;
use App\Mail\UserEmailVerification;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationJoin;
use Illuminate\Support\Facades\DB;
use Veritrans_Config;
use Veritrans_Notification;
use Veritrans_Snap;

class PaymentController extends Controller
{
    /**
     * Make request global.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;
 
    /**
     * Class constructor.
     *
     * @param \Illuminate\Http\Request $request User Request
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
 
        // Set midtrans configuration
        Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $payment = Payment::whereCompany_id($request->company_id)->get();
        
        $response = [
            'status' => 'success',
            'data' => $payment
        ];
        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $payment = Payment::with('company')
                ->whereCompany_id($request->company_id)
                ->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $payment
        ];

        return response()->json($response, 200);
    }

    public function getViewOrderPayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'no_payment' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $payment = Payment::with('company')
                ->whereNo_payment($request->no_payment)
                ->firstOrFail();

        return view('order-payment', $payment->toArray());

    }

    public function storeOrderPayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'no_payment' => 'required',
            'via' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $payment = Payment::whereNo_payment($request->no_payment)->firstOrFail();

        // for promo
        if ($payment->total_price == 0) {
            // 1 month
            $date = Carbon::now()->addMonth()->toDateString();
            $date_sendinv = date('Y-m-d', strtotime('-7 days', strtotime($date)));

            //due date for non active company
            $due_date = date('Y-m-d', strtotime('+7 days', strtotime($date)));
        
        // for non promo
        } else {
            // 3 months
            $date = Carbon::now()->addMonths(3)->toDateString();
            $date_sendinv = date('Y-m-d', strtotime('-7 days', strtotime($date)));

            //due date for non active company
            $due_date = date('Y-m-d', strtotime('+7 days', strtotime($date)));
        }

        $payment->update([
            'via' => $request->via,
            'date_paid' => Carbon::today(),
            'date_sendinv' => $date_sendinv,
            'status' => 1,
        ]);

        //update status company to active
        $company = Company::findOrFail($payment->company_id);
        $company->update([
            'due_date' => $due_date,
            'status' => 1
        ]);


        $count = Payment::whereCompany_id($payment->company_id)->count();

        //this is for registration user
        if ($count < 2) {
             //update status user pending
             $user = $company->users()->latest()->first();
             $user->update([
                 'status' => 1
             ]);

            //TODO batas waktu pembayaran gimana ?
 
             //Temporary url 
             $url_activation = URL::temporarySignedRoute('user.activation', now()->addDays(30),[
                 'email' => $user->email,
             ]);
             
             $details = [
                 'to' => $user->email,
                 'name' => $user->name,
                 'email' => $user->email,
                 'url_activation' => $url_activation
             ];
 
             //send email to Admin
             Mail::to($user->email)->send(new UserEmailVerification($details));
 
             $details = [
                 'to' => $company->email,
                 'name' => $company->name,
                 'email' => $user->email,
             ];
 
             //send email to Company
             Mail::to($company->email)->send(new NotificationJoin($details));

        }

        return response()->json([
            'status' => 'success',
            'message' => 'Payment successfully.',
            'data' => $payment
        ],200);

    }

    public function midtransPaymentGetSnapToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'no_payment' => 'required',
        ]);

        if($validator->fails()){ 
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $payment = Payment::with('company')->whereNo_payment($request->no_payment)->first();

        if ($payment->snap_token == null) {
            $payload = [
                'transaction_details' => [
                    'order_id'      => $payment->no_payment,
                    'gross_amount'  => $payment->total_price,
                ],
                'customer_details' => [
                    'first_name'    => $payment->company->name,
                    'email'         => $payment->company->email,
                    'phone'         => $payment->company->phone,    
                    'address'       => $payment->company->address,
                ],
                'expiry' => [
                    'unit' => 'days',           
                    'duration' => 365
                ]
            ];
            $snap_token = Veritrans_Snap::getSnapToken($payload);

            $payment->update(['snap_token' => $snap_token]);
        }

        return response()->json([
            'status' => 'success',
            'snap_token' => $payment->snap_token
        ], 200);
        
    }

    public function notificationHandler(Request $request)
    {
        $notif = new Veritrans_Notification();
        $status_code = $notif->status_code;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;

        $payment = Payment::with('company')->whereNo_payment($order_id)->first();

        if ($status_code == 200) {
            
            // for promo
            if ($payment->total_price == 0) {
                // 1 month
                $date = Carbon::now()->addMonth()->toDateString();
                $date_sendinv = date('Y-m-d', strtotime('-7 days', strtotime($date)));

                //due date for non active company
                $due_date = date('Y-m-d', strtotime('+7 days', strtotime($date)));
            
            // for non promo
            } else {
                // 3 months
                $date = Carbon::now()->addMonths(3)->toDateString();
                $date_sendinv = date('Y-m-d', strtotime('-7 days', strtotime($date)));

                //due date for non active company
                $due_date = date('Y-m-d', strtotime('+7 days', strtotime($date)));
            }

            if (substr($order_id, 0,3) == 'DIG') {
                $payment->update([
                    'via' => $type,
                    'date_paid' => Carbon::today(),
                    'date_sendinv' => $date_sendinv,
                    'status' => 1,
                ]);

                //update status company to active
                $company = Company::findOrFail($payment->company_id);
                $company->update([
                    'due_date' => $due_date,
                    'status' => 1,
                ]);
            }

            // update payment after add user action
            if (substr($order_id, 0,3) == 'ADD') {
                $payment->update([
                    'via' => $type,
                    'date_paid' => Carbon::today(),
                    'status' => 1,
                ]);

                $company = Company::findOrFail($payment->company_id);
                $company->update([
                    'number_user' => $company->number_user + $payment->number_user,
                    'max_user' => $company->max_user + $payment->number_user,
                ]);
            }
    
            $count = Payment::whereCompany_id($payment->company_id)->count();
    
            //this is for registration user
            if ($count < 2) {
                 //update status user pending
                 $user = $company->users()->latest()->first();
                 $user->update([
                     'status' => 1
                 ]);
     
                 //Temporary url 
                 $url_activation = URL::temporarySignedRoute('user.activation', now()->addDays(30),[
                     'email' => $user->email,
                 ]);
                 
                 $details = [
                     'to' => $user->email,
                     'name' => $user->name,
                     'email' => $user->email,
                     'url_activation' => $url_activation
                 ];
     
                 //send email to Admin
                 Mail::to($user->email)->send(new UserEmailVerification($details));
     
                 $details = [
                     'to' => $company->email,
                     'name' => $company->name,
                     'email' => $user->email,
                 ];
     
                 //send email to Company
                 Mail::to($company->email)->send(new NotificationJoin($details));
    
            }
        }

        return response()->json([
            'status' => 'success',
        ],200);
   
    }

    public function revenue(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $result =  DB::table('payments')->select(DB::raw('SUM( CASE WHEN MONTH ( date_paid ) = 1 THEN total_price ELSE 0 END ) AS total_revenue_january,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 1 THEN company_id ELSE NULL END ) AS total_company_january,
                SUM( CASE WHEN MONTH ( date_paid ) = 1 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 1 THEN company_id ELSE NULL END )  as average_january,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 2 THEN total_price ELSE 0 END ) AS total_revenue_february,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 2 THEN company_id ELSE NULL END ) AS total_company_february,
                SUM( CASE WHEN MONTH ( date_paid ) = 2 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 2 THEN company_id ELSE NULL END )  as average_february,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 3 THEN total_price ELSE 0 END ) AS total_revenue_march,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 3 THEN company_id ELSE NULL END ) AS total_company_march,
                SUM( CASE WHEN MONTH ( date_paid ) = 3 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 3 THEN company_id ELSE NULL END )  as average_march,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 4 THEN total_price ELSE 0 END ) AS total_revenue_april,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 4 THEN company_id ELSE NULL END ) AS total_company_april,
                SUM( CASE WHEN MONTH ( date_paid ) = 4 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 4 THEN company_id ELSE NULL END )  as average_april,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 5 THEN total_price ELSE 0 END ) AS total_revenue_mei,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 5 THEN company_id ELSE NULL END ) AS total_company_mei,
                SUM( CASE WHEN MONTH ( date_paid ) = 5 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 5 THEN company_id ELSE NULL END )  as average_mei,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 6 THEN total_price ELSE 0 END ) AS total_revenue_june,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 6 THEN company_id ELSE NULL END ) AS total_company_june,
                SUM( CASE WHEN MONTH ( date_paid ) = 6 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 6 THEN company_id ELSE NULL END )  as average_june,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 7 THEN total_price ELSE 0 END ) AS total_revenue_july,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 7 THEN company_id ELSE NULL END ) AS total_company_july,
                SUM( CASE WHEN MONTH ( date_paid ) = 7 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 7 THEN company_id ELSE NULL END )  as average_july,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 8 THEN total_price ELSE 0 END ) AS total_revenue_august,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 8 THEN company_id ELSE 0 END ) AS total_company_august,
                SUM( CASE WHEN MONTH ( date_paid ) = 8 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 8 THEN company_id ELSE NULL END )  as average_august,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 9 THEN total_price ELSE 0 END ) AS total_revenue_september,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 9 THEN company_id ELSE NULL END ) AS total_company_september,
                SUM( CASE WHEN MONTH ( date_paid ) = 9 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 9 THEN company_id ELSE NULL END )  as average_september,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 10 THEN total_price ELSE 0 END ) AS total_revenue_october,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 10 THEN company_id ELSE NULL END ) AS total_company_october,
                SUM( CASE WHEN MONTH ( date_paid ) = 10 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 10 THEN company_id ELSE NULL END )  as average_october,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 11 THEN total_price ELSE 0 END ) AS total_revenue_november,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 11 THEN company_id ELSE NULL END ) AS total_company_november,
                SUM( CASE WHEN MONTH ( date_paid ) = 11 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 11 THEN company_id ELSE NULL END )  as average_november,
                
                SUM( CASE WHEN MONTH ( date_paid ) = 12 THEN total_price ELSE 0 END ) AS total_revenue_december,
                COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 12 THEN company_id ELSE NULL END ) AS total_company_december,
                SUM( CASE WHEN MONTH ( date_paid ) = 12 THEN total_price ELSE 0 END ) / COUNT( DISTINCT CASE WHEN MONTH ( date_paid ) = 12 THEN company_id ELSE NULL END )  as average_december'))
                ->whereYear('date_paid', $request->year)
                ->whereStatus(1)
                ->first();
        
        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);
    }

}
