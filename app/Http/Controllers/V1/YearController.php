<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class YearController extends Controller
{
    public function index()
    {
        $today = Carbon::now();
        $year = $today->year;

        $ranges = range(2019,$year);
        $result = [];

        foreach ($ranges as $range) {
            array_push($result,[
                'year' => $range
            ]);
        }

        $response = [
            'status' => 'success',
            'data' => $result
        ];
        return response()->json($response, 200);

        
    }
}
