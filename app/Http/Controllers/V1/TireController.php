<?php

namespace App\Http\Controllers\V1;

use App\Models\Tire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tire = Tire::all();
        $response = [
            'status' => 'success',
            'data' => $tire
        ];
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'position' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $tire = Tire::create($request->all());
        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $tire
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tire = Tire::findOrFail($id);
        $response = [
            'status' => 'success',
            'data' => $tire
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'position' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $tire = Tire::findOrFail($id);
        $tire->update($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $tire
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tire = Tire::findOrFail($id);
        $tire->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}
