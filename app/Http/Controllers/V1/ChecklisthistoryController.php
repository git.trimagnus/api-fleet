<?php

namespace App\Http\Controllers\V1;

use App\Models\Armada;
use App\Models\Checklisthistory;
use App\Models\Maintenance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ChecklisthistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable|exists:armadas,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Checklisthistory::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
  
        $query = $query->whereStatus(1);
        $query = $query->orderBy('id', 'DESC');
        $checklisthistory = $query->get();
        $checklisthistory->load(['armada', 'maintenance', 'checklistvehicle']);

        $response = [
            'status' => 'success',
            'data' => $checklisthistory
        ];
        return response()->json($response, 200);

    }

    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $checklisthistory = Checklisthistory::with(['armada', 'maintenance', 'checklistvehicle'])
                        ->whereCompany_id($request->company_id)
                        ->whereArmada_id($request->armada_id)
                        ->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $checklisthistory
        ];
        return response()->json($response, 200);

    }

}
