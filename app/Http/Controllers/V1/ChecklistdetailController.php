<?php

namespace App\Http\Controllers\V1;

use App\Models\Maintenance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Checklisthistory;
use App\Models\Checklistdetail;

class ChecklistdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'maintenance_id' => 'required|exists:maintenances,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Checklistdetail::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('maintenance_id')) {
            $query = $query->whereMaintenance_id($request->maintenance_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
  
        $checklistdetail = $query->get();
        $checklistdetail->load('checklistvehicle');

        $response = [
            'status' => 'success',
            'data' => $checklistdetail
        ];
        return response()->json($response, 200);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Maintenance = Maintenance::findOrFail($id);
        $checklistdetails = Checklistdetail::with(['checklistvehicle'])
                        ->whereMaintenance_id($Maintenance->id)
                        ->get();

        $response = [
            'status' => 'success',
            'checklistdetails' => $checklistdetails
        ];
        return response()->json($response, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'checklistdetails.*.checklistvehicle_id' => 'required|exists:checklistvehicles,id',
            'checklistdetails.*.detail' => 'present|nullable|string',
            'checklistdetails.*.status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $maintenance = Maintenance::findOrFail($id);
        Checklisthistory::insert($maintenance->checklistdetails()->get()->toArray());
        $maintenance->checklistdetails()->forceDelete();

        foreach ($request->checklistdetails as $checklistdetail) {
            $maintenance->checklistdetails()->create([
                'company_id' => $maintenance->company_id,
                'armada_id' => $maintenance->armada_id,
                'maintenance_id' => $maintenance->maintenance_id,
                'checklistvehicle_id' => $checklistdetail['checklistvehicle_id'],
                'detail' => $checklistdetail['detail'],
                'status' => $checklistdetail['status']
            ]);
        }
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'checklistdetails' => Maintenance::findOrFail($id)->checklistdetails
        ];
        return response()->json($response, 200);
    }
    
}
