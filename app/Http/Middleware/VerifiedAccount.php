<?php

namespace App\Http\Middleware;

use Closure;

class VerifiedAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $status_company = auth()->user()->company->status;
        $status_user = auth()->user()->status;

        //check if company non active
        if ($status_company != 1) {
            return response()->json([
                'status' => 'error',
                'message' => 'Your company account has been disabled, please complete the payment.'
            ], 403);
        }

        //check if user non active
        if ($status_user != 2) {
            return response()->json([
                'status' => 'error',
                'message' => 'Your account has been disabled.'
            ], 403);
        }

        return $next($request);
    }
}
