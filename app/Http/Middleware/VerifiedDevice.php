<?php

namespace App\Http\Middleware;

use Closure;

class VerifiedDevice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->headers->has('device')) {
            $device = auth()->user()->device;
            if ($device != $request->header('device')) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Detected an unusual login attempt.'
                ], 401);
            } else {
                return $next($request);            
            }
            
        }

        return $next($request);
    }
}
