<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// API V1
Route::group(['prefix' => 'v1'], function() {

  Route::get('mail', 'V1\AuthController@mail');

  Route::post('contact-us', 'V1\HomeController@contactUs');
  
  Route::post('forgot-password/email', 'V1\PasswordController@sendResetLinkEmail');
  Route::post('forgot-password/reset', 'V1\PasswordController@reset')->name('reset.password');

  Route::group(['prefix' => 'users'], function() {
    Route::post('login', 'V1\AuthController@login');
    Route::post('register', 'V1\AuthController@register');
    Route::get('activation', 'V1\AuthController@verification');
    Route::get('check-email', 'V1\AuthController@checkEmail');
  });

  Route::apiResource('subscriptions', 'V1\SubscriptionController');

  Route::apiResource('payments', 'V1\PaymentController')->only('index', 'show');
  Route::post('payments/notification/handler', 'V1\PaymentController@notificationHandler');

  Route::get('revenues', 'V1\PaymentController@revenue');

  Route::get('order-payment/snap-token', 'V1\PaymentController@midtransPaymentGetSnapToken')->name('get.snap_token');
  Route::post('order-payment', 'V1\PaymentController@storeOrderPayment')->name('store.order.payment');

//These route can be used to only allow authenticated users and verified account
Route::middleware(['jwt.auth', 'verified.device', 'verified.account'])->group(function () {

  //Dashboard
  Route::get('dashboard', 'V1\DashboardController@admin');
  Route::get('dashboard/super-admin', 'V1\DashboardController@superAdmin');

  //Home 
  Route::group(['prefix' => 'home'], function() {
    Route::get('orders', 'V1\HomeController@getAllOrders');
    Route::get('maintenances', 'V1\HomeController@getAllMaintenances');
    Route::get('orderhistories', 'V1\HomeController@getAllOrderHistories');
    Route::get('maintenancehistories', 'V1\HomeController@getAllMaintenanceHistories');
    Route::get('invoicehistories', 'V1\HomeController@getAllInvoiceHistories');
  });
  
  //User Management 
  Route::group(['prefix' => 'users'], function() {
    Route::get('refresh', 'V1\AuthController@refresh'); //Refresh token
    Route::get('me', 'V1\AuthController@me'); //Get User Login 
    //change-password user login
    Route::put('change-password', 'V1\PasswordController@change');
    //change-password by user_id
    Route::put('{user_id}/change-password', 'V1\PasswordController@changePassword');
    Route::apiResource('super-admin', 'V1\User\SuperAdminController');
    Route::apiResource('admin', 'V1\User\AdminController');
    Route::apiResource('driver', 'V1\User\DriverController');
    Route::post('driver-form', 'V1\User\DriverController@storeForm');
    Route::put('driver-form/{user_id}', 'V1\User\DriverController@updateForm');
    Route::apiResource('workshop', 'V1\User\WorkshopController');
    Route::put('{user_id}/status', 'V1\User\UserController@updateStatus');
  });

  //Schedule  
    Route::get('schedules/armada-available', 'V1\ScheduleController@scheduleArmadaAvailable');
    Route::get('schedules/driver-available', 'V1\ScheduleController@scheduleDriverAvailable');
    Route::get('schedules/armada-order', 'V1\ScheduleController@scheduleArmadaOrder');

    Route::apiResource('notifications', 'V1\NotificationController');
    Route::apiResource('companies', 'V1\CompanyController');
    Route::post('companies/user-additional','V1\CompanyController@userAdditional');
    Route::get('companies/{company_id}/add-user-transaction','V1\CompanyController@addUserTransaction');
    Route::get('companies/{company_id}/add-user-transaction/{id}','V1\CompanyController@getOneAddUserTransaction');

    Route::apiResource('itemdetails', 'V1\ItemdetailController');
    
    Route::apiResource('customers', 'V1\CustomerController');
    Route::apiResource('roles', 'V1\RoleController');
    Route::apiResource('costs', 'V1\CostController');
    Route::apiResource('simtypes', 'V1\SimtypeController');

    Route::apiResource('armadas', 'V1\ArmadaController');
    Route::get('armada-maintenance', 'V1\ArmadaController@armadaMaintenance');
    Route::put('armadas/{armada_id}/status', 'V1\ArmadaController@updateStatus');

    Route::apiResource('armadareminders', 'V1\ArmadareminderController');
    Route::apiResource('tirehistories', 'V1\TirehistoryController');
    Route::apiResource('checklisthistories', 'V1\ChecklisthistoryController');
    Route::apiResource('tires', 'V1\TireController');
    Route::apiResource('tireconditions', 'V1\TireconditionController');
    Route::apiResource('tirecatalogs', 'V1\TirecatalogController');
    Route::apiResource('odometerhistories', 'V1\OdometerhistoryController');

    Route::apiResource('checklistvehicles', 'V1\ChecklistvehicleController');
    Route::apiResource('maintenancetypes', 'V1\MaintenancetypeController');
    Route::apiResource('maintenances', 'V1\MaintenanceController');
    Route::get('maintenances/status/open', 'V1\MaintenanceController@maintenanceStatusOpen');
    Route::get('maintenances/status/progress', 'V1\MaintenanceController@maintenanceStatusProgress');
    Route::get('maintenances/status/history', 'V1\MaintenanceController@maintenanceStatusHistory');
    Route::put('maintenances/{maintenance_id}/status', 'V1\MaintenanceController@updateStatus');
    Route::get('checklistdetails', 'V1\ChecklistdetailController@index');
    Route::put('maintenances/{maintenance_id}/checklistdetails', 'V1\ChecklistdetailController@update');
   
    Route::apiResource('orders', 'V1\OrderController');
    Route::get('orders/status/progress', 'V1\OrderController@orderStatusProgress');
    Route::get('orders/status/pending', 'V1\OrderController@orderStatusPending');
    Route::get('orders/status/history', 'V1\OrderController@orderStatusHistory');
    
    // amin
    Route::get('orders/detail/getItemDetail','V1\OrderController@getItemCode');

    Route::get('orders/driver/status/open', 'V1\OrderController@orderDriverStatusOpen');
    Route::get('orders/driver/status/progress', 'V1\OrderController@orderDriverStatusProgress');
    Route::get('orders/driver/status/loaded', 'V1\OrderController@orderDriverStatusLoaded');
    Route::get('orders/driver/status/delivered', 'V1\OrderController@orderDriverStatusDelivered');
    Route::get('orders/driver/status/verified', 'V1\OrderController@orderDriverStatusVerified');
  

    Route::put('orders/{order_id}/status', 'V1\OrderController@updateStatus');
    Route::apiResource('billingdetails', 'V1\BillingdetailController');
    Route::get('orders/{order_id}/rating', 'V1\OrderController@showRating');
    Route::put('orders/{order_id}/rating', 'V1\OrderController@updateRating');
    Route::get('orders/{order_id}/budgetdetails', 'V1\BudgetdetailController@show');
    Route::put('orders/{order_id}/budgetdetails', 'V1\BudgetdetailController@update');

    Route::put('orders/{id}/statusDetail', 'V1\OrderController@updateStatusDetail');

    
    Route::get('orders/{order_id}/sj', 'V1\OrderController@sj');
    Route::get('orders/{order_id}/inv', 'V1\OrderController@inv');
    // amin tambah generate tanda terima
    Route::get('orders/{order_id}/tandaterima', 'V1\OrderController@tandaterima');
    Route::get('invoices', 'V1\OrderController@invoices');

    //Report
    Route::get('reports/armada-performance', 'V1\ReportController@armadaPerformance');
    Route::get('reports/fuel-report', 'V1\ReportController@fuelReport');
    Route::get('reports/insurance-performance', 'V1\ReportController@insurancePerformance');
    Route::get('reports/delivery-order', 'V1\ReportController@deliveryOrder');
    Route::get('reports/workshop-performance', 'V1\ReportController@workshopPerformance');
    Route::get('reports/external-workshop', 'V1\ReportController@externalWorkshop');
    Route::get('reports/rating-driver', 'V1\ReportController@ratingDriver');
    Route::get('reports/outstanding-driver', 'V1\ReportController@outstandingDriver');
    Route::get('reports/outstanding-customer', 'V1\ReportController@outstandingCustomer');
    Route::get('reports/profit-and-loss', 'V1\ReportController@profitAndLoss');

    Route::get('years', 'V1\YearController@index');

  });
  
});

// API V2
Route::group(['prefix' => 'v2'], function () {

  Route::get('onmedia-all','V2\OnmediaController@get_all');
  Route::get('event-all','V2\EventController@get_all');
  Route::get('updates-all','V2\UpdatesController@get_all');

  //These route can be used to only allow authenticated users and verified account
  Route::middleware(['jwt.auth', 'verified.device', 'verified.account'])->group(function () {
    Route::apiResource('armadas', 'V2\ArmadaController');
    Route::apiResource('tiredetails', 'V2\TiredetailController');

    // Event
    Route::apiResource('events', 'V2\EventController');
    // Onmedia
    Route::apiResource('onmedia', 'V2\OnmediaController');
    // Updates
    Route::apiResource('updates', 'V2\UpdatesController');

  });
    
});






