<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return response()->json([
        "Project" => "Api Fleet Management",
        "Framework" => "Laravel 5.8.*",
        "Created_by" => "Evan Nada Virgiawan",
        "Company" => "PT. Trimagnus Prima Dharma"
    ], 200);
});

// Page Password Reset
Route::get('forgot-password/reset/{token}', function ($token) {
    return view('email-reset', ['token' => $token]);
})->name('forgot-password.reset')->middleware('signed');

// Page Success Activation
Route::get('users/activation', 'V1\AuthController@verification')
    ->name('user.activation')
    ->middleware('signed');

// Page Order Payment
Route::get('order-payment', 'V1\PaymentController@getViewOrderPayment')
    ->name('order.payment')->middleware('signed');
