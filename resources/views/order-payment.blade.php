<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Order Payment</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('assets/modules/sweetalert2/sweetalert2.min.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .btn-danger,
        .btn-danger.disabled {
            box-shadow: 0 2px 6px #acb5f6;
            background-color: #Da2827;
            border-color: #Da2827;
        }

    </style>
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-6 offset-3">
                        <div class="login-brand">
                            <img src="{{URL::to('icons/logo.png')}}" alt="Logo" width="140px">
                        </div>

                        <div class="card card-danger">
                            <div class="card-header">
                                <h4>Order Payment</h4>
                            </div>

                            <div class="card-body">
                                <form method="post" id="form-order">
                                    <input id="via" type="hidden" name="via" value="">
                                    @csrf
                                    @method('POST')
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="no_payment">No. Pembayaran</label>
                                                <input id="no_payment" type="text" class="form-control"
                                                    name="no_payment" value="{{$no_payment}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="no_payment">Nama Perusahaan</label>
                                                <input id="no_payment" type="text" class="form-control"
                                                    name="companies['name']" value="{{$company['name']}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="email">Email Perusahaan</label>
                                                <input id="#" type="text" class="form-control" name="companies['email']"
                                                    value="{{$company['email']}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="#">No. Tlp Perusahaan</label>
                                                <input id="#" type="text" class="form-control" name="companies['phone']"
                                                    value="{{$company['phone']}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table table-striped" id="order-datatable">
                                                    <thead>
                                                        <tr>
                                                            <th>Jumlah User</th>
                                                            <th>Unit Price</th>
                                                            <th>Total Price</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>{{ $number_user }}</td>
                                                            <td>{{ $unit_price }}</td>
                                                            <td>{{ $total_price }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      @if ($status != 1)
                                          <button type="submit" id="pay" class="btn btn-danger btn-lg btn-block" tabindex="4">
                                              Checkout
                                          </button>
                                      @else
                                          <button type="submit" class="btn btn-danger btn-lg btn-block" tabindex="4" disabled>
                                              Paid
                                          </button>
                                      @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="simple-footer">
                            Copyright &copy; 2019 PT Fitlogindo Mega Tama All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- General JS Scripts -->
    <script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('assets/modules/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/stisla.js') }}"></script>

    <!-- JS Libraies -->
  <script src="{{ asset('assets/modules/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Template JS File -->
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
    <script type="text/javascript">
        // $(function () {
        //     $('#form-order').on('submit', function (e) {
        //         if (!e.isDefaultPrevented()) {
        //             $.ajax({
        //                 url: "{{ route('store.order.payment') }}",
        //                 type: "POST",
        //                 data: $('#form-order').serialize(),
        //                 beforeSend: function() {
        //                   swal({
        //                       title: 'Now loading',
        //                       allowEscapeKey: false,
        //                       allowOutsideClick: false,
        //                       timer: 30000,
        //                       onOpen: () => {
        //                       swal.showLoading();
        //                       }
        //                   })
        //                 },
        //                 success: function (response) {
        //                     swal({
        //                         title: 'Success!',
        //                         text: response.message,
        //                         type: 'success',
        //                         timer: '1500'
        //                     })
        //                     location.reload();
        //                 },
        //                 error: function (response) {
        //                     swal({
        //                         title: 'Opps...',
        //                         text: response.responseText,
        //                         type: 'error',
        //                         timer: '2000'
        //                     })
        //                     location.reload();
        //                 }
        //             });
        //             return false;
        //         }
        //     });
        // });

        $('#pay').click(function(){

          // Kirim request ajax
          $.post("{{ route('get.snap_token') }}",
          {
            _method: 'GET',
            _token: '{{ csrf_token() }}',
            no_payment: '{{$no_payment}}',
          },
          function (response) {
              snap.pay(response.snap_token, {
                  // Optional
                  onSuccess: function (result) {
                    location.reload();
                  },
                  // Optional
                  onPending: function (result) {
                    location.reload();
                  },
                  // Optional
                  onError: function (result) {
                    location.reload();
                  }
              });
          });
        return false;

        });

    </script>

</body>

</html>
