<!DOCTYPE html>
<html>

<head>
    <title>User Activation</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <style>
        button.btn-lg:hover {
            color: #fff;
            border-radius: 30px;
            border-color: #fff;
            background-color: #da261d;
        }

        button.btn-lg {
            color: #da261d;
            border-radius: 30px;
            border-color: #da261d;
            background-color: #fff;
        }

        .sosmed {
            margin-top: 2%;
            margin-left: 40%;
            margin-right: 40%;
        }

        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #da261d;
            color: white;
            text-align: center;
        }

        @media (max-width: 980px) {

            /*.text{
					background: url(ilustrasi.png);
				}*/
            /*.kanan{
					display: none
				}*/
            .text {
                text-align: center;
                padding-top: 5% !important;
                padding-bottom: 5%;
            }

            .sosmed {
                margin-left: 35%;
                margin-right: 35%;
            }

            .services {
                margin-bottom: 8%;
            }
        }

        @media (max-width: 400px) {
            .services {
                margin-bottom: 18%;
            }

            .sosmed {
                margin-left: 0%;
                margin-right: 0%;
            }
        }

    </style>
</head>

<body style="background: #fff;">
    <div class="container-fluid">
        <div class="head">
            <div class="row" style="padding-top: 50px;">
                <div class="col-lg-1 col-md-1 col-sm-0"></div>
                <div class="col-lg-6 col-md-12 col-sm-12 kiri" style="padding-left: %;">
                    <div class="logo">
                        <img src="{{URL::to('other/logo.png')}}" style="height: 70px; width: auto;">
                    </div>
                    <div class="text" style="text-transform: capitalize; padding-top: 20%;">
                        <h1>Selamat</h1>
                        <h3>akun anda telah diaktifkan</h3>
                        <h3>silahkan login melalui aplikasi <b>digkontrol</b></h3>
                        <div class="row" style="padding-top: 5%;">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <button class="btn btn-lg" style="width: 100%;"
                                    onclick="location='http://app.digkontrol.com'">Masuk Web</button>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <button class="btn btn-lg" style="width: 100%;" onclick="location='https://play.google.com/store/apps/details?id=com.digkontrol.app'">Download App</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 kanan">
                    <img src="{{URL::to('other/ilustrasi.png')}}" style="width: 100%; float: left;">
                </div>
            </div>
        </div>
        <section class="sosmed">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 item">
                        <center>
                            <a href="https://facebook.com/Digkontrol" target="_blank"><img
                                    src="{{URL::to('other/fb.png')}}"></a>
                        </center>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 item">
                        <center>
                            <a href="https://instagram.com/digkontrol?igshid=1eiqoqjwhcpcf" target="_blank"><img
                                    src="{{URL::to('other/ig.png')}}"></a>
                        </center>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 item">
                        <center>
                            <a href="https://id.linkedin.com/company/fitlogindo-megatama?trk=public_profile_topcard_current_company"
                                target="_blank"><img src="{{URL::to('other/link.png')}}"></a>
                        </center>
                    </div>
                </div>
            </div>
        </section>
        <section class="services">
            <div class="container">
                <div class="row">
                    <a href="http://digkontrol.com/service" style="margin-left: 8px;margin-right: 8px">FAQ</a>
                    <a href="http://digkontrol.com/service" style="margin-left: 8px;margin-right: 8px">About US</a>
                    <a href="http://digkontrol.com/service" style="margin-left: 8px;margin-right: 8px">Term Of Use</a>
                    <a href="http://digkontrol.com/service" style="margin-left: 8px;margin-right: 8px">Privacy
                        Policy</a>
                </div>
            </div>
        </section>
        <div class="footer">
            <p>
                <h6>© PT Fitlogindo Mega Tama</h6>
            </p>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>

</html>
