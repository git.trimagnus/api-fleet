@component('mail::message')
# Hi, {{$owner_name}}

## {{$message}}

@component('mail::button', ['url' => $url_order_payment])
Klik disini untuk ke Halaman Pembayaran
@endcomponent



Salam Kontrol, <br>
Tim {{ config('app.name') }}
@endcomponent