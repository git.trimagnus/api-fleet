@component('mail::message')
# Hi, {{$owner_name}}

@component('mail::button', ['url' => $url_order_payment])
Klik disini untuk ke Halaman Pembayaran
@endcomponent



Thanks, <br>
Tim {{ config('app.name') }}
@endcomponent
