@component('mail::message')
# Hi, {{ $name }}

Terimakasih Telah mendaftar <b>Digkontrol</b> <br>
Silahkan klik tombol dibawah ini
untuk melakukan verifikasi akun anda 

@component('mail::button', ['url' => $url_activation])
Klik disini untuk verifikasi email
@endcomponent

Atau copy and paste link dibawah ini di browser Anda <br>
@php
$subject = $url_activation;
$search = 'amp;signature';
$trimmed = str_replace($search, 'signature', $subject);
echo $trimmed;
@endphp <br>

Terima kasih telah menggunakan Aplikasi Digkontrol.<br> 
Untuk kemudahan Anda, kami menyediakan manual books yang bisa anda akses / download, setelah anda Log In di Aplikasi Web, dibagian pojok kanan atas, tombol User Account.

Thanks, <br>
Tim {{ config('app.name') }}

@endcomponent
